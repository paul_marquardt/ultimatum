import ultimatum.*

def performanceService = new PerformanceService()

performanceService.deletePerformance()
println "I just deleted a performance before creating a new one"

def newPerformance = performanceService.createPerformance("Test Performance",500)

newPerformance.getErrors().each { error ->
    println "Error: ${error}"
}

def performerService = new PerformerService()
def aPerformer = performerService.createPerformer("linda", newPerformance)

aPerformer.getErrors().each { error ->
    println "Error: ${error}"
}

println( "Application name: ${grailsApplication.metadata['app.name']}")
//newPerformance.addToPerformers( aPerformer )
//aPerformer = performerService.createPerformer("joe", newPerformance)
//newPerformance.addToPerformers( aPerformer )

Performance.list().each { perf ->
    thisPerf = perf.get(perf.id)
    println "Performance Name: ${thisPerf.name}"
    performers = thisPerf.performers
    println "Performers in ${thisPerf.name}"
    performers.each { performer ->
        println "\tPerformer: ${performer.name} :: ${performer.role}"
    }
}

println "=================="
performanceService.deletePerformance()
println "I just deleted a performance"

Performance.list().each { perf ->
    thisPerf = perf.get(perf.id)
    println "Name: ${thisPerf}"
}