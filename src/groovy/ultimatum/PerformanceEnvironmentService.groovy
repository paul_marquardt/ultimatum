package ultimatum

import grails.util.Holders

class PerformanceEnvironmentService {
	def performanceEnvironment
	
	PerformanceEnvironment getPerformanceEnvironment() {
		if ( performanceEnvironment == null )  {
			//PerformanceEnvironment.withTransaction {
			log.info("No performanceEnvironment bean found by injection, will search persistence...")
			if ( PerformanceEnvironment.findById( 0 ) != null ) {
				log.info("PerformanceEnvironment instance found in persistence, returning the first one...")
				return PerformanceEnvironment.findById( 0 )
			}
			log.info("No PerformanceEnvironment found in persistence, creating PerformanceEnvironment with values osc://${Holders.config.ultimatum.osc.remoteHost}:${Holders.config.ultimatum.osc.remotePort}/${Holders.config.ultimatum.osc.applicationId}")
			return null
		} else {
			log.info("Found existing PerformanceEnvironment, returning it...")
			return performanceEnvironment
		}
	}
	
	PerformanceEnvironment createPerformanceEnvironment() {
		if ( performanceEnvironment == null || PerformanceEnvironment.findById( 0 ) == null )  {
			//PerformanceEnvironment.withTransaction {
			log.info("No PerformanceEnvironment found, creating PerformanceEnvironment with values osc://${Holders.config.ultimatum.osc.remoteHost}:${Holders.config.ultimatum.osc.remotePort}/${Holders.config.ultimatum.osc.applicationId}")
			performanceEnvironment = new PerformanceEnvironment(
				oscRemoteHost : Holders.config.ultimatum.osc.remoteHost,
				oscRemotePort : Holders.config.ultimatum.osc.remotePort,
				applicationId : Holders.config.ultimatum.osc.applicationId
			)
			//performanceEnvironment.lock()
			try {
				performanceEnvironment.save()
			} catch ( Exception exc ) {
				log.error("Error saving PerformanceEnvironment: ${exc.message}")
			}
			//}
			return performanceEnvironment
		} else {
			log.info("Found existing PerformanceEnvironment, will not create a new instance...")
		}
	}
	
	PerformanceEnvironment createPerformanceEnvironment(String remoteHost, Integer remotePort, String applicationId) {
		if ( performanceEnvironment == null || PerformanceEnvironment.findById( 0 ) == null )  {
			//PerformanceEnvironment.withTransaction {
			log.info("No PerformanceEnvironment found, creating PerformanceEnvironment with values osc://${Holders.config.ultimatum.osc.remoteHost}:${Holders.config.ultimatum.osc.remotePort}/${Holders.config.ultimatum.osc.applicationId}")
			performanceEnvironment = new PerformanceEnvironment(
				oscRemoteHost : remoteHost,
				oscRemotePort : remotePort,
				applicationId : applicationId
			)
			try {
				performanceEnvironment.save()
			//}
			} catch ( Exception e) {
				log.error( "Unable to save performanceEnvironment: ${e.message}")
			}
			//}
			return performanceEnvironment
		} else {
			log.info("Found existing PerformanceEnvironment, will not create a new instance...")
		}
	}

	PerformanceEnvironment modifyPerformanceEnvironment( String host, Integer port, String applicationId ) {
		def performanceEnvironment = getPerformanceEnvironment()
		if ( performanceEnvironment != null )  {
			//PerformanceEnvironment.withTransaction {
			performanceEnvironment.oscRemoteHost = host
			performanceEnvironment.oscRemotePort = port
			performanceEnvironment.applicationId = applicationId
			//performanceEnvironment = port
			//performanceEnvironment.lock()
			try {
				performanceEnvironment.save()
			//}
			} catch ( Exception e) {
				log.error( "Unable to save performanceEnvironment: ${e.message}")
			}
			return performanceEnvironment
		} else {
			log.info("Modify failed:: no existing PerformanceEnvironment was found.")
		}
	}

}
