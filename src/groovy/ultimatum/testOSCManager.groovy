package ultimatum

import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Random  
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.illposed.osc.*;
//import de.sciss.netutil.*

class TestOSCManager {

    public static final int DEFAULT_OSC_PORT = 57120
    public static final String DEFAULT_OSC_HOST = "localhost"
    public static final String DEFAULT_OSC_APPLICATION = "/ultimatum"
    String remoteHost
    int remotePort
    String instrumentName
    String applicationId
    private InetAddress remoteAddress
    //private OSCPortOut oscPort
    //private OSCClient oscClient
    protected Log logger
    def testRuns = 100

    def TestOSCManager( String inRemoteHost, int inRemotePort, String inApplicationId ) throws UnknownHostException, IllegalArgumentException, SocketException {
        this.applicationId = inApplicationId
        logger = LogFactory.getLog(getClass())
        this.remoteHost = inRemoteHost
        this.remotePort = inRemotePort
        remoteAddress = InetAddress.getByName(this.remoteHost)
        try {
            //this.oscPort = new OSCPortOut( remoteAddress, this.remotePort )
            //this.oscClient = new OSCClient.newUsing( OSCClient.UDP )
            //this.oscClient.setTarget( new InetSocketAddress( inRemoteHost, inRemotePort ) );
            //this.oscClient.start();  // open channel and (in the case of TCP) connect, then start listening for replies

        } catch (java.net.SocketException exc) {
            logger.error(exc.message)
        }
    }
    
    public void sendMessage( Object messageToSend, Long instrumentId ) {
        Random rand = new Random()  
        Integer challengeId = rand.nextInt()
        //def instrumentName = String.format( "instr%d_%d", instrumentId, challengeId );
        def instrumentName = String.format( "instr" );
        //def bundle   = new OSCBundle( System.currentTimeMillis() );
        //OSCMessage message = new OSCMessage( "/${this.applicationId}" )
        //OSCPacket bundle = new OSCPacket();
        //message.addArgument(instrumentName)
        //message.addArgument(messageToSend)
        //message.addArgument( String.format( "challenge%d", challengeId ) )
        //message.addArgument( challengeId )
        //bundle.addPacket(message);
        try {
            //bundle.addPacket( new OSCMessage( "/ultimatum", [ "instr", instrumentId, "challenge", challengeId ] ) )
            //this.oscClient.send( bundle );
            print( "sent OSC message OSC message with address osc://${this.remoteHost}:${this.remotePort}/${this.applicationId}/instr${instrumentId}/${messageToSend}" )
        } catch (java.net.SocketException exc) {
            print( exc.localizedMessage )
            print("could not send OSC message with address osc://${this.remoteHost}:${this.remotePort}/${this.applicationId}/instr${instrumentId}/${messageToSend}")
        }
    }
    
    public void sendChallengeMessage( Long instrumentId ) {
        this.sendMessage("challenge", instrumentId )
    }
    
    public void sendFinishedMessage( Long instrumentId ) {
        // yes, the "finished" message is actually "done" on the OSC server.
        this.sendMessage("done", instrumentId)
    }
    
    public void sendPuntMessage( Long instrumentId ) {
        this.sendMessage("punt", instrumentId)
    }
	
	public void sendGlobalMessages( String parameterName, String parameterValue ) {
		Random rand = new Random()
		Integer challengeId = rand.nextInt()
		//def instrumentName = String.format( "instr%d_%d", instrumentId, challengeId );
		def instrumentName = String.format( "instr" );
		def bundle   = new OSCBundle( System.currentTimeMillis() );
		OSCMessage message = new OSCMessage( "/${this.applicationId}" )
		//OSCPacket bundle = new OSCPacket();
		//message.addArgument(instrumentName)
		//message.addArgument(messageToSend)
		//message.addArgument( String.format( "challenge%d", challengeId ) )
		//message.addArgument( challengeId )
		//bundle.addPacket(message);
		try {
			//bundle.addPacket( new OSCMessage( "/ultimatum", [ "instr", instrumentId, "challenge", challengeId ] ) )
			//this.oscClient.send( bundle );
			print( "sent OSC message OSC message with address osc://${this.remoteHost}:${this.remotePort}/${this.applicationId}/instr${instrumentId}/${messageToSend}" )
		} catch (java.net.SocketException exc) {
			print( exc.localizedMessage )
			print("could not send OSC message with address osc://${this.remoteHost}:${this.remotePort}/${this.applicationId}/instr${instrumentId}/${messageToSend}")
		}
	}

}


TestOSCManager oscManApp = new TestOSCManager( "localhost", 57120, "ultimatum-app" )

oscManApp.sendChallengeMessage( 1L )