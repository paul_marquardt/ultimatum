package ultimatum

class ChallengeFactory {
	static createChallenge(Performer source, Performer target, String type, String message, Long challengeId) {
		return new GroovyChallenge( [source : source, target : target, type : type, message : message, id : challengeId ] )
	}
}

class ChallengeManager {

	String name
	List<GroovyChallenge> challenges
	Long currentId
	
	ChallengeManager() {
		challenges = new ArrayList<GroovyChallenge>()
		currentId = 0
	}
	
	GroovyChallenge sendChallenge(Performer source, Performer target, String type, String message) {
		def gchall = ChallengeFactory.createChallenge(source, target, type, message, currentId)
		log.info("ChallengeManager set a GroovyChallenge from ${source.name} to ${target.name}")
		challenges.add(gchall)
		currentId++
		return gchall
	}
		
	GroovyChallenge consumeChallenge(Long challengeId) {
		def groovyChallenge = challenges.find { item ->
			item.id == challengeId
		}
		challenges.removeAll({ item ->
			item.id == challengeId
		})
		return groovyChallenge
	}
}
