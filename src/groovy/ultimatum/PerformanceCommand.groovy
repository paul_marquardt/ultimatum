package ultimatum

import java.util.Date;
import grails.util.Holders


class PerformanceCommand {
	String name
	String passcode
	Double duration
	Date startTime
	boolean isStarted = false

	static constraints = {
		passcode(nullable : true)
		startTime(nullable : true)
		isStarted(nullable : true)
		performanceEnvironment.oscRemoteHost(nullable : true)
		performanceEnvironment.oscRemotePort(nullable : true)
		performanceEnvironment.ApplicationId(nullable : true)
	}

}
