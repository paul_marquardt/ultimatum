package ultimatum

import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.illposed.osc.*;

class OSCManager {

	public static final int DEFAULT_OSC_PORT = 57120
	public static final String DEFAULT_OSC_HOST = "localhost"
	public static final String DEFAULT_OSC_APPLICATION = "/ultimatum"
	String remoteHost
	int remotePort
	String instrumentName
	String applicationId
	private InetAddress remoteAddress
	private OSCPortOut oscPort
	protected Log logger
	
	def OSCManager( String inRemoteHost, int inRemotePort, String inApplicationId ) throws UnknownHostException, IllegalArgumentException, SocketException {
		this.applicationId = inApplicationId
	    logger = LogFactory.getLog(getClass())
		this.remoteHost = inRemoteHost
		this.remotePort = inRemotePort
		remoteAddress = InetAddress.getByName(this.remoteHost)
		try {
			this.oscPort = new OSCPortOut( remoteAddress, this.remotePort )
		} catch (java.net.SocketException exc) {
			logger.error(exc.message)
		}
	}

	def OSCManager( PerformanceEnvironment pe ) throws UnknownHostException, IllegalArgumentException, SocketException {
		this.applicationId = pe.applicationId
		this.remoteHost = pe.oscRemoteHost
		this.remotePort = pe.oscRemotePort
		logger = LogFactory.getLog( getClass() )
		remoteAddress = InetAddress.getByName(this.remoteHost)
		try {
			this.oscPort = new OSCPortOut( remoteAddress, this.remotePort )
		} catch (java.net.SocketException exc) {
			logger.error(exc.message)
		}
	}

	public void sendMessage( Object messageToSend, Long instrumentId, Long challengeId ) {
		def instrumentName = String.format( "instr%d_%d", instrumentId, challengeId );
		//def challengeString = String.format( "%d", challengeId );
		//Object[] args = new Object[2]
		//args[0] = instrumentName
		//args[1] = messageToSend
		def subAddress = "app";
		try {
            OSCMessage message = new OSCMessage( "/${this.applicationId}-${subAddress}" )
			//OSCBundle bundle = new OSCBundle();
			message.addArgument(instrumentName)
			message.addArgument(messageToSend)
			//bundle.addPacket(message);
			//message.addArgument("instr2")
			// need to convert to Integer. SuperCollider has nasty bug that crashes if sent a Long
			//message.addArgument( challengeId )
			this.oscPort.send(message)
			logger.error( "sent OSC message OSC message with address osc://${this.remoteHost}:${this.remotePort}/${this.applicationId}/${instrumentName}/${messageToSend}" )
		} catch (Exception exc) {
			logger.error( exc.message )
			logger.error("could not send OSC message with address osc://${this.remoteHost}:${this.remotePort}/${this.applicationId}-${subAddress}/${instrumentName}/${messageToSend}")
		}
	}

	public void sendMessage( String subAddress, Object messageToSend, Long instrumentId, Long challengeId ) {
		def instrumentName = String.format( "instr%d_%d", instrumentId, challengeId );
		//def challengeString = String.format( "%d", challengeId );
		//Object[] args = new Object[2]
		//args[0] = instrumentName
		//args[1] = messageToSend
		try {
			OSCMessage message = new OSCMessage( "/${this.applicationId}-${subAddress}" )
			//OSCBundle bundle = new OSCBundle();
			message.addArgument(instrumentName)
			message.addArgument(messageToSend)
			//bundle.addPacket(message);
			//message.addArgument("instr2")
			// need to convert to Integer. SuperCollider has nasty bug that crashes if sent a Long
			//message.addArgument( challengeId )
			this.oscPort.send(message)
			logger.error( "sent OSC message OSC message with address osc://${this.remoteHost}:${this.remotePort}/${this.applicationId}-${subAddress}/${instrumentName}/${messageToSend}" )
		} catch (Exception exc) {
			logger.error( exc.message )
			logger.error("could not send OSC message with address osc://${this.remoteHost}:${this.remotePort}/${this.applicationId}-${subAddress}/${instrumentName}/${messageToSend}")
		}
	}

	/*
	 * For sending Global messages, it is important that receiving application knows how to process the message.
	 * This method is only a messenger and makes no guarantee what the server will do with it. Due to the limitations of sending
	 * complex data to OSC, the message should be in the form of "param=value". For example, setting the performanceTime parameter to 600
	 * would look something like "/global/performanceTime=600"
	 *
	 */
	public void sendGlobalMessage( String parameterName, Object value ) {
		def subAddress = "global";
		//def challengeString = String.format( "%d", challengeId );
		//Object[] args = new Object[2]
		//args[0] = instrumentName
		//args[1] = messageToSend
		try {
			OSCMessage message = new OSCMessage( "/${this.applicationId}-${subAddress}" )
			//OSCBundle bundle = new OSCBundle();
			//message.addArgument(instrumentName)
			message.addArgument(parameterName)
			message.addArgument(value)
			//bundle.addPacket(message);
			//message.addArgument("instr2")
			// need to convert to Integer. SuperCollider has nasty bug that crashes if sent a Long
			//message.addArgument( challengeId )
			this.oscPort.send(message)
			logger.error( "sent OSC message OSC message with address osc://${this.remoteHost}:${this.remotePort}/${this.applicationId}-${subAddress}/${parameterName}/${value}" )
		} catch (Exception exc) {
			logger.error( exc.message )
			logger.error("could not send OSC message with address osc://${this.remoteHost}:${this.remotePort}/${this.applicationId}-${subAddress}/${parameterName}/${value}")
		}
	}

	public void sendChallengeMessage( Long instrumentId, Long challengeId ) {
		this.sendMessage("app", "challenge", instrumentId, challengeId)
	}
	
	public void sendFinishedMessage( Long instrumentId, Long challengeId ) {
		// yes, the "finished" message is actually "done" on the OSC server.
		this.sendMessage("app", "done", instrumentId, challengeId)
	}
	
	public void sendPuntMessage( Long instrumentId, Long challengeId ) {
		this.sendMessage("app", "punt", instrumentId, challengeId)
	}

	public void sendPuntAndResponse( Long instrumentId, Long challengeId ) {
		this.sendMessage("app", "puntAndResponse", instrumentId, challengeId)
	}

	public void recordBuffer( ) {
		logger.info("Sent a record message")
		this.sendMessage( "record", "record", 9999, 9999)
	}

		/*
	 * sendClosingMessage is the message sent to trigger the sample signaling the closing of the performance.
	 * It is different that the sendFinishedMessage indicating a response to the challenge.
	 * 
	 */
	public void sendClosingMessage() {
		// yes, the "finished" message is actually "done" on the OSC server.
		// Send a fake instrumentID and challengeID, since these values are not relevant for the "done" message.
		this.sendMessage("app", "finish", 1, 0)
	}

}
