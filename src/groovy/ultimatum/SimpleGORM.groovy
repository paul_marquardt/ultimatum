import ultimatum.*

def newPerf = new Performance( name : "Simple Performance", duration : 600 )
newPerf.save()

def performer = new Performer( name : "Steve", role : "leader", performance : newPerf )
performer.save()

Performer.list()[0].name