import ultimatum.*


def performanceService = new PerformanceService()

def newPerformance = performanceService.getPerformance()

if ( newPerformance != null ) {
    println "I got a Performance named ${newPerformance.name}"
}

performanceService.deletePerformance()
println "I just deleted a performance before creating a new one"

newPerformance = performanceService.createPerformance("Test Performance",500)

newPerformance.getErrors().each { error ->
    println "Error: ${error}"
}

def performerService = new PerformerService()
def leader = performerService.createPerformer("linda", newPerformance)
def player1 = performerService.createPerformer("linda", newPerformance)
def player2 = performerService.createPerformer("linda", newPerformance)

[ leader, player1, player2 ].each { perf ->
    if ( perf.hasErrors() ) {
        println "Performer ${perf.name} has errors:"
        perf.getErrors().each { error ->
            println "Error: ${error}"
        }
    }
}

//performanceService.deletePerformance()
//println "I just deleted a performance after it was created"