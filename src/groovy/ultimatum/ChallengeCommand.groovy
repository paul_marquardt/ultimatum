package ultimatum

class ChallengeCommand {

	Performer source
	Performer target
	String message
	
	static constraints = {
		source(
			validator: { challengeCommand ->
				return challengeCommand.source.id != challengeCommand.target.id
			})
		
		target(
			validator: { challengeCommand ->
				return challengeCommand.target.id != challengeCommand.source.id
			})

	}
}
