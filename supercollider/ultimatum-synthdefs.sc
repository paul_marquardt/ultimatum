(
	SynthDef( \samplePlayer, {
		arg outbus = 0, bufnum = 0, amp = 1.0, scale = 0.5, startPos = 0,
		freqScale = 1.0, attack = 0.001, duration = 2.0, pan = 0.5;
		Out.ar(
			outbus,
			Pan2.ar(
				PlayBuf.ar(numChannels:1, bufnum:bufnum, rate: BufRateScale.kr(bufnum) * freqScale,
					trigger:1, startPos:startPos, loop:0 ) *
				//EnvGen.kr( Env.perc( attack, duration, amp, 8 ), doneAction: 2 ),
				EnvGen.kr( Env.cutoff( 0.5 ), doneAction: 2 ),
				pan
			)
		);
	}).send(s);

	SynthDef(\compander,
		{
			arg outBus = 0, in, thr = 0.8, slBelow = 1.5, slAbove = 0.5;
			Out.ar(
				outBus,
				Compander.ar(
					In.ar( in ),
					In.ar( in ),
					thresh: thr,
					slopeBelow: slBelow,
					slopeAbove: slAbove,
					clampTime: 0.01,
					relaxTime: 0.01
				)
				//Normalizer.ar(
				//	PlayBuf.ar(1, bufNum, BufRateScale.kr(bufNum), doneAction:2),
				//	0.5,
				//	0.1
				//)
			)
	}).send(s);

	SynthDef( \low_pass,
		{
			arg output = 0, in, rq = 1.5, lf_freq = 1.0, freq = 1000, add = 2000;
			Out.ar(
				output,
				RLPF.ar(
					in: In.ar( in ),
					freq: LFNoise2.kr( freq: lf_freq, mul: freq, add: add ),
					rq: 1.5
				)
			);
		}
	).send(s);

	SynthDef( \sampleShifter,
		{
			arg output = 0, bufnum = 0, amp = 1.0, scale = 0.5, pitchDispersion = 0.001, panRate = 1/4,
			windowSize = 0.15, attack = 0.001, duration = 16.0, freq = 1.0;
			Out.ar(
				//output,
				[0,1],
				PitchShift.ar(
					PanB2.ar(
						PlayBuf.ar(1, bufnum, BufRateScale.kr(bufnum) * scale, 1, Server.local.sampleRate * 0.5, 0) *
						EnvGen.kr( Env.sine( duration, amp ), doneAction: 7 ),
						LFNoise1.kr( panRate )
					),
					windowSize,
					scale,
					pitchDispersion,
					freq
					//Rand( 0.01, 1.2 )
				)
			);
		}
	).send(s);

	SynthDef(\pv_morph, { arg outBus=0, in1, in2, xfadeRate;
		var inA, chainA, inB, chainB, chain;
		chainA = FFT(LocalBuf(2048, 1), In.ar( in1 ) );
		chainB = FFT(LocalBuf(2048, 1), In.ar( in2 ) );
		chain = PV_Morph(chainA, chainB, LFNoise2.kr( 0.1 ), LFNoise2.kr( xfadeRate ) );
		Out.ar(
			outBus,
			Pan2.ar(
			IFFT(chain).dup * EnvGen( Env.sine ),
				LFNoise2.kr( 0.2 )
			)
		)
	}).send(s);

	SynthDef( \sampleShifter_new,
		{
			arg output = 0, bufnum = 0, maxAmp = 1.0, scale = 0.5, pitchDispersion = 0.001, panRate = 1/8,
			envLevels = #[0,1,0], envTimes = #[1.0,1.0], windowSize = 0.15, attack = 0.001, duration = 16.0, freq = 1.0, attackT = 0.2, releaseT = 0.5, env, factor = 0.5, envCurve = 'sine';
			//env = Env.linen(attackT, duration - (attackT + releaseT), releaseT, 1, 'lin');
			env = Env.new( levels: envLevels, times: envTimes, curve: envCurve );
			Out.ar(
				//output,
				[0,1],
				PitchShift.ar(
					PanB2.ar(
						PlayBuf.ar(
							numChannels: 1,
							bufnum: bufnum,
							rate: BufRateScale.kr(bufnum) * scale,
							trigger: 1,
							startPos: Server.local.sampleRate * 0.5,
							loop: 1) *
						EnvGen.kr(
							envelope: env,
							gate: 1,
							levelScale: maxAmp,
							levelBias: 0,
							timeScale: 1,
							doneAction: 7
						),
						LFNoise1.kr( panRate )
					),
					windowSize,
					scale,
					pitchDispersion,
					freq
					//Rand( 0.01, 1.2 )
				)
			);
		}
	).send(s);

	SynthDef(\rand_wipe, { arg outBus=0, in1, in2, duration;
		var inA, chainA, inB, chainB, chain;
		chainA = FFT(LocalBuf(2048, 1), In.ar( in1 ) );
		chainB = FFT(LocalBuf(2048, 1), In.ar( in2 ) );
		chain = PV_RandWipe(chainA, chainB, LFNoise2.kr( 0.5 ), LFNoise2.kr( 0.5 ) > 0.5);
		Out.ar(outBus, IFFT(chain).dup * EnvGate.new );
	}).send(s);

	SynthDef(\soft_wipe, { arg outBus=0, in1, in2, amp = 0.5, scale = 0.5, pitchDispersion = 0.1, panRate = 1/8,
		windowSize = 0.25, attack = 0.01, duration = 16.0, freq = 1.0;
		var inA, chainA, inB, chainB, chain;
		chainA = FFT(LocalBuf(2048, 1), In.ar( in1 ) );
		chainB = FFT(LocalBuf(2048, 1), In.ar( in2 ) );
		chain = PV_SoftWipe(chainA, chainB, LFNoise2.kr( 2.0 ) );
		Out.ar(outBus,
			PitchShift.ar(
				Pan2.ar(
					IFFT(chain),
					LFNoise1.kr( 0.8 )
				).dup,
				windowSize,
				LFNoise2.kr( 0.5, 0.1, 1.0 ),
				pitchDispersion,
				freq
			)
		);
	}).send(s);

	SynthDef(\rand_comb, { arg outBus=0, in1, in2,
		envLevels = #[0,1,0], envTimes = #[1.0,1.0], combRate = 0.5, maxAmp = 1.0, envCurve = 'sine';
		var inA, chainA, inB, chainB, chain, env;
		env = Env.new( levels: envLevels, times: envTimes, curve: envCurve );
		chainA = FFT(LocalBuf(2048, 1), In.ar( in1 ) );
		chainB = FFT(LocalBuf(2048, 1), In.ar( in2 ) );
		chain = PV_RandComb(chainA, chainB, LFNoise2.kr( 0.5 ), LFNoise2.kr( combRate ) > 0.5);
		Out.ar(outBus, IFFT(chain).dup *
			EnvGen.kr(
				envelope: env,
				gate: 1,
				levelScale: maxAmp,
				levelBias: 0,
				timeScale: 1,
				doneAction: 7 )
		) ;
	}).send(s);

	SynthDef(\xfade, {
		arg outBus=0, in1, in2, xfadeRate = 1/8,
		windowSize = 0.25, attack = 0.01, freq = 1.0,
		envLevels = #[0,1,0], envTimes = #[1.0,1.0];
		var inA, chainA, inB, chainB, chain;
		chainA = FFT(LocalBuf(2048, 1), In.ar( in1 ) );
		chainB = FFT(LocalBuf(2048, 1), In.ar( in2 ) );
		chain = PV_XFade(chainA, chainB, LFNoise2.kr( xfadeRate ) );
		Out.ar(outBus, IFFT(chain).dup);
	}).send(s);

	SynthDef(\diffuser, { arg outBus=0, in1, in2;
		var inA, chainA, inB, chainB, chain;
		chainA = FFT(LocalBuf(2048, 1), In.ar( in1 ) );
		chain = PV_Diffuser(chainA, LFNoise2.kr( 0.5 ) );
		Out.ar(outBus, IFFT(chain).dup);
	}).send(s);

	SynthDef( \panningSamplePlayer, {
		arg outbus = 0, bufnum = 0, amp = 1.0, scale = 0.5, startPos = 0,
		freqScale = 1.0, attack = 0.001, duration = 2.0, pan = LFNoise1.kr( 0.1 );
		Out.ar(
			outbus,
			Pan2.ar(
				PlayBuf.ar(numChannels:1, bufnum:bufnum, rate: BufRateScale.kr(bufnum) * freqScale,
					trigger:1, startPos:startPos, loop:0 ) *
				//EnvGen.kr( Env.perc( attack, duration, amp, 8 ), doneAction: 2 ),
				EnvGen.kr( Env.cutoff( 0.5 ), doneAction: 2 ),
				pan
			)
		);
	}).send(s);

	SynthDef( \recordBuffer, {
		arg input = 1, buf = 0, doneAction = 2;
		var audioIn = AudioIn.ar( input );
		RecordBuf.ar( inputArray:[ audioIn ], bufnum:buf, offset:0, recLevel:1.0, preLevel:0, run:1, trigger:1, doneAction: doneAction );
	}).send(s);
)