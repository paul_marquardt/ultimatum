(
~synthdefFilePath = [thisProcess.nowExecutingPath.dirname,
	"ultimatum-synthdefs.sc"].join("/").standardizePath;
//var configFilePath;
//Server.killAll;
s = Server.default;
s.boot;

r = Routine({
	s.sync;
	~synthdefFilePath.load;
    s.sync; // assuming that 's' is the server
	"you sukc".postln;
	~configFilePath = [thisProcess.nowExecutingPath.dirname,
		"ultimatum-test.yaml"].join("/").standardizePath;

	~mandatoryKeys = ["appId", "addr", "appDirectory" ];
	~file = File(~configFilePath, "r");
	if ( ~file.isNil, {
		"Unable to find config file at %s".format( ~configFilePath );
		1.exit;
		}, {
			"Found config file at %s".format( ~configFilePath );
	});

	~configFilePath.postln;
	~appConfig = ~configFilePath.parseYAMLFile;
	~mandatoryKeys.do({
		| key |
		if( ~appConfig.at(key).isNil, {
			"Value missing for key %s in file %s. Exiting".format(key, ~configFilePath);
			},{
				"Found value '%s' for key %s".format( ~appConfig.at(key), key);
		});
	});
	// use this address in private network environment.
	~outbus = 0;
	~numinstruments = 10;
	// use this address when testing on normal network environment.
	//addr = "ultimatum-host";
	~addr = ~appConfig.at("addr");
	~appId = ~appConfig.at("appId");

	~appDir = ~appConfig.at("appDirectory").standardizePath;
	~tempSfDir = "%/%".format( ~appDir, ~appId ).standardizePath;
	// delete all files in tempSfDir before each performance, as they're performance-specific.
	"rm -f %/*".format(~tempSfDir).unixCmd;
	~uPlayer = UltimatumPlayer.new( ~addr, 57120, ~outbus, ~appId, ~numinstruments, ~appDir );
	//~uPlayer.testPerformance("ultimatum-test");
	//60.wait;
	~uPlayer.startPerformance("ultimatum-test");

});

SystemClock.sched( 0.0, r );
)