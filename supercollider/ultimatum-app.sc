(
~synthdefFilePath = [thisProcess.nowExecutingPath.dirname,
	"ultimatum-synthdefs.sc"].join("/").standardizePath;
//var configFilePath;
//Server.killAll;
s = Server.default;
s.boot;

Routine({
	s.sync;
	~synthdefFilePath.load;
    s.sync; // assuming that 's' is the server
	"you sukc".postln;
	~configFilePath = [thisProcess.nowExecutingPath.dirname,
		"ultimatum.yaml"].join("/").standardizePath;

	~mandatoryKeys = ["appId", "addr", "appDirectory" ];
	~file = File(~configFilePath, "r");
	if ( ~file.isNil, {
		"Unable to find config file at %s".format( ~configFilePath );
		1.exit;
		}, {
			"Found config file at %s".format( ~configFilePath );
	});

	~configFilePath.postln;
	~appConfig = ~configFilePath.parseYAMLFile;
	~mandatoryKeys.do({
		| key |
		if( ~appConfig.at(key).isNil, {
			"Value missing for key %s in file %s. Exiting".format(key, ~configFilePath);
			},{
				"Found value '%s' for key %s".format( ~appConfig.at(key), key);
		});
	});
	// use this address in private network environment.
	~outbus = 0;
	~numinstruments = 10;
	// use this address when testing on normal network environment.
	//addr = "ultimatum-host";
	~addr = ~appConfig.at("addr");
	~appId = ~appConfig.at("appId");

	~appDir = ~appConfig.at("appDirectory").standardizePath;
	~tempSfDir = "%/%/%".format( ~appDir, "temp_sounds" ).standardizePath;
	"listing files in %".format( "%/*.wav".format(~tempSfDir) ).postln;
	// delete all files in tempSfDir before each performance, as they're performance-specific.
	//"rm -f %/*".format(~tempSfDir).unixCmd;
	~tempSfPath = PathName.new( ~tempSfDir );
	~tempSfPath.files.do({
		| path |
		var file;
		//file = File.new( path.fullPath );
		//file.delete;
	});
	~uPlayer = UltimatumPlayer.new( ~addr, 57120, ~outbus, ~appId, ~numinstruments, ~appDir );
}).play;

)