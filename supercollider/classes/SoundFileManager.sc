SoundFileManager : Object {

	var <>fileDict, <>sfDirectory, <>server, <>puntFileBuffers, <>finishedFileBuffers, <>puntCount;
	
	init { | dir , server |
		this.puntCount = 0;
		this.fileDict = Dictionary.new;
		this.puntFileBuffers = List.new;
		this.finishedFileBuffers = List.new;
		this.sfDirectory = dir;
		this.server = server;
		this.prepareSpeechFiles;
		this.prepareSoundFiles;
		^this;
	}
	
	*new { | dir, server |
		^super.new.init( dir, server );
	}

	prepareSpeechFiles {
		var match, sfIndex, filename2, files, speechFileDir;
		speechFileDir = this.sfDirectory.asString ++ "/speech";
		files = (speechFileDir ++ "/if_you_*").standardizePath.pathMatch;
		"listing contents of %".format( speechFileDir ).postln;
		if ( files != nil, {
			files.do ({
				| filename1, index |
				"% : %".format(filename1, index).postln;
				match = filename1.findRegexp("/if_you_([0-9]+).wav$");
				//match.postln;
				if( match != nil && match.at(1).size > 0 && match.at(1).size == 2, {
					var indexString;
					indexString = match.at(1).at(1);
				//if( ".*if_you_[0-9]?\.wav".matchRegexp(filename), {
					//"Match % : %".format(filename, match.at(1).at(1) ).postln;
					//filename.postln;
					"%/one_more_time_%.wav".format( speechFileDir.standardizePath.asString, match.at(1).at(1) ).postln;
					filename2 = "%/one_more_time_%.wav".format(speechFileDir.standardizePath.asString, indexString );
					if ( File.exists(filename1) && File.exists(filename2), {
							"File pair % : %".format(filename1, filename2).postln;
							this.fileDict.add( indexString -> [ Buffer.read(this.server, filename1), Buffer.read(this.server, filename2) ] );
					});
					match = nil;
				});
			});
		});
	}
	
	prepareSoundFiles {
		|puntFileDir, puntFilePaths, files, finishedFileDir, finishedFilePaths|
		puntFileDir = this.sfDirectory.asString ++ "/sounds/*";
		//files = puntFileDir.standardizePath.pathMatch;
		//this.puntFiles = SoundFile.collect(puntFileDir);
		puntFilePaths = ("%/sounds/*.wav").format(this.sfDirectory.asString).standardizePath.pathMatch;
		puntFilePaths.do({
			|filename|
			"Found a punt file at %".format(filename).postln;
			this.puntFileBuffers.add( Buffer.read(this.server, filename) );
		});
		finishedFilePaths = ("%/sounds_final/*.wav").format(this.sfDirectory.asString).standardizePath.pathMatch;
		finishedFilePaths.do({
			|filename|
			"Found a finish file at %".format(filename).postln;
			this.finishedFileBuffers.add( Buffer.read(this.server, filename) );
		});
	}
	
	getRandomBufferPair {
		^this.fileDict.values.choose;
	}
	
	getRandomSoundBuffer {
		// OK. so it's not completely random after all...
		var match;
		match = nil;
		if ( this.puntCount != 3, {
			this.puntCount = this.puntCount + 1;
			^this.puntFileBuffers.choose;
		}, {
			this.puntFileBuffers.do({
				| buffer |
				match = buffer.path.findRegexp("/MillsTapeCenter_01.wav$");
				if ( match != nil, {
					this.puntCount = this.puntCount + 1;
					^buffer;
				});
			});
			this.puntCount = this.puntCount + 1;
			^this.puntFileBuffers.choose;
		});
	}

	getRandomFinishedBuffer {
		^this.finishedFileBuffers.choose;
	}

	
	getAllBuffers {
		^this.files;
	}
}