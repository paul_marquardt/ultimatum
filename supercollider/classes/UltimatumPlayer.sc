UltimatumPlayer : Object {

	var <>name, <>networkAddr, <>appId, <>oscPortStart, <>oscResponders, <>outbus, <>numInstruments, <>server, <>sfManager, <>speechBufferPair, <>performanceDuration, <>performanceStartTime, <>performanceName, <>tempSfDir, <>sfStagingDir, <>appDir, <>appConfig, <>isWritingToDisk, <>pvOutBus, <>mainEffectBus, <>mainOutBus, <>initialRecordWait = 15, <>initialPlaybackWait = 30;

	init { | address, port, outbus, appId = "ultimatum", numInstruments, appDir |
		//appDirectory = "/Users/marqrdt/Music/composition/ultimatum";
		this.networkAddr = address;
		this.oscPortStart = port;
		this.outbus = outbus;
		this.appId = appId;
		this.appDir = appDir;
		this.numInstruments = numInstruments;
		// defaut to 720 seconds, or 12 minutes
		this.performanceDuration = 720;
		// A sort of semaphore to avoid the rare case where a sound file is opened for reading
		// before it's flushed to disk.
		this.isWritingToDisk = false;
		this.server = Server.default;
		/* define the reusable Buses here to avoid reallocating each time
		the sampler player runs...
		*/
		this.pvOutBus = Bus.audio( this.server, 2 );
		this.mainEffectBus = Bus.audio( this.server, 2 );
		this.mainOutBus = Bus.audio( this.server, 2 );
		this.sfManager = SoundFileManager.new( appDir, this.server );
		this.tempSfDir = [appDir, "temp_sounds"].join("/").standardizePath;
		"rm -f %s/*.wav".format(this.tempSfDir).unixCmd;
		this.sfStagingDir = [appDir, "staging"].join("/").standardizePath;
		"rm -f %s/*.wav".format(this.sfStagingDir).unixCmd;
		this.createOSCresponders;
		^this;
	}

	*new { | address, port, outbus = 0, instruments, sfDir, tempSfDir |
		^super.new.init( address, port, outbus = 0, instruments, sfDir, tempSfDir );
	}
	createOSCresponders {
		var netAddr, commands, cmdString, port, oscResponder;
		//netAddr = NetAddr(this.networkAddr, this.oscPortStart);
		// try creating only one set of responders for all instruments instead of
		// one responder per instrument. might solve the problem where two WAV files
		// are played per command. the comdString can just be "/ultimatum" instead of
		// "ultimatum/instr1", etc.
		netAddr = NetAddr("127.0.0.1", NetAddr.langPort);
		this.speechBufferPair = this.sfManager.getRandomBufferPair;
		"creating OSC responder...".postln;
		OSCFunc.newMatching( {
			|msg, time, addr, netAddr|
			//"% : %".format(t,msg).postln;
			Routine.new({
				this.globalMessage(time,msg);
			}).play;
		}, '/ultimatum-global' ); // path matching
		OSCFunc.newMatching( {
			|msg, time, addr, netAddr|
			//"% : %".format(t,msg).postln;
			Routine.new({
				//rrand(0.01,0.015).wait;
				this.playWav(time,msg);
			}).play;
		}, '/ultimatum-app' ); // path matching
		OSCFunc.newMatching( {
			|msg, time, addr, netAddr|
			//"% : %".format(t,msg).postln;
			Routine.new({
				//rrand(0.01,0.015).wait;
				this.recordBufferOSC(time,msg);
			}).play;
		}, '/ultimatum-record' ); // path matching
	}

	testOSC {
		|t,msg|
		"OSC message received: % : %".format(msg,t).postln;
	}

	startPerformance {
		| perfName |
		var companderBus, companderSynth, samplePlaybackBus, lowPassSynth;
		this.performanceName = perfName;
		this.performanceStartTime = SystemClock.seconds;
		"Performance % has been started at % with duration %".format( this.performanceName, this.performanceStartTime, this.performanceDuration );
		~minSampleRecordTrigger = 20.0;
		~maxSampleRecordTrigger = 45.0;
		~minSamplePlayTrigger = 20.0;
		~maxSamplePlayTrigger = 45.0;
		~sampleDuration = rrand( 8.0, 15.0 );

		companderBus = Bus.audio(this.server, 2);
		samplePlaybackBus = Bus.audio(this.server, 2);
		/*
		Basic routing:
		~pvEffectBus -> ~samplePlaybackBus -> ~companderBus -> Out
		*/
		// 	arg output = 0, in, rq = 1.5, lf_freq = 1.0, freq = 100, add = 2000;

		companderSynth = Synth.new( \compander, [ \outBus, 0, \in, companderBus, \thr, 0.8] );
		lowPassSynth = Synth.before( companderSynth, \low_pass, [ \outBus, companderBus, \in, samplePlaybackBus, \rq, 1.5, \lf_freq, 1/2, \freq, 300, \add: 2000] );

		/*
		Run the Task to schedule the recordBuffer method. It should start running
		approximately 20.0 seconds after the performance start and run between
		~minSampleRecordTrigger and ~maxSampleRecordTrigger seconds until
		60 seconds before the performance ends.
		*/
		Task({
			var ambientBus, ambientController;
			2.0.wait;
			ambientBus = Bus.audio(this.server, 2);
			ambientController = AmbientController.new(
				[
					this.appDir,
					"ambient_sounds"
				].join("/").standardizePath,
				ambientBus
			);
			ambientController.start;
		}).play;

		Task({
			// wait 60 seconds before recording the first buffer.
			this.initialRecordWait.wait;
			{ SystemClock.seconds < (this.performanceStartTime + this.performanceDuration - 60.0); }.while(
				{
					var waitTime;
					// waitTime is a random value between ~minSampleRecordTrigger and ~maxSampleRecordTrigger
					waitTime = (~maxSampleRecordTrigger - ~minSampleRecordTrigger).rand + ~minSampleRecordTrigger;
					this.recordBuffer( ~sampleDuration, 0 );
					waitTime.wait;
				}
			);
		}).play;
		// This Task schedule the synths object that play back the buffer recorded from the live performance.
		Task({
			// wait 90 seconds before running the task. They will run
			this.initialPlaybackWait.wait;
			{ SystemClock.seconds < (this.performanceStartTime + this.performanceDuration - 30.0); }.while(
				{
					var waitTime;
					// waitTime is a random value between ~minSampleRecordTrigger and ~maxSampleRecordTrigger
					waitTime = (~maxSamplePlayTrigger - ~minSamplePlayTrigger).rand + ~minSampleRecordTrigger;
					/**
					playRecordedSamples method should only need to know about
					~lowPassSynth and ~samplePlaybackBus. Needs ~lowPassSynth so it can place
					the PV Synth before this Synth in the group.
					**/
					~dur = rrand( 15, 45 );
					this.playRecordedSamples(lowPassSynth, samplePlaybackBus, ~dur);
					waitTime.wait;
				}
			);
		}).play;

	}

	testPerformance {
		| perfName |
		var companderBus, companderSynth, samplePlaybackBus, lowPassSynth;
		this.performanceName = perfName;
		this.performanceStartTime = SystemClock.seconds;
		"Testing performance %".format( this.performanceName );

		companderBus = Bus.audio(this.server, 2);
		samplePlaybackBus = Bus.audio(this.server, 2);
		/*
		Basic routing:
		~pvEffectBus -> ~samplePlaybackBus -> ~companderBus -> Out
		*/
		// 	arg output = 0, in, rq = 1.5, lf_freq = 1.0, freq = 100, add = 2000;

		companderSynth = Synth.new( \compander, [ \outBus, 0, \in, companderBus, \thr, 0.8] );
		lowPassSynth = Synth.before( companderSynth, \low_pass, [ \outBus, companderBus, \in, samplePlaybackBus, \rq, 1.5, \lf_freq, 1/2, \freq, 300, \add: 2000] );

		Task({
			// wait 60 seconds before recording the first buffer.
			var msg, time;
			time = SystemClock.seconds;
			4.0.wait;
			// msg[0] -> the application name (should be /ultimatum)
			// msg[1] -> the source instrument (instrX where X -> 0..n)
			// msg[2] -> the command, one of 'challenge', 'done', 'punt', 'finish'
			// msg[3] -> the challenge id. A unique number assigned to each challenge unit

			// instrIdMatch = msg.at(1).asString.findAllRegexp(".*instr([0-9]+)_");
			// The challengeId is the second numerical value (2) in the string instr1_2
			// challengeIdMatch = msg.at(1).asString.findAllRegexp(".*instr[0-9]+_([0-9]+)");

			// test use case: challenge from instr[0], response from instr[1]
			msg = [ "/ultimatum", "instr_0_0", "challenge", 1];
			this.playWav(time,msg);
			4.0.wait;
			msg = [ "/ultimatum", "instr_1_0", "done", 1];
			this.playWav(time,msg);
			4.0.wait;
			// test use case: challenge from instr[1], response from instr[0]
			msg = [ "/ultimatum", "instr_1_1", "challenge", 1];
			this.playWav(time,msg);
			4.0.wait;
			msg = [ "/ultimatum", "instr_0_1", "done", 1];

			4.0.wait;
			// test use case: punt from instr[0], punt from instr[1]
			msg = [ "/ultimatum", "instr_0_2", "punt", 1];
			this.playWav(time,msg);
			15.0.wait;
			msg = [ "/ultimatum", "instr_1_2", "punt", 1];
			this.playWav(time,msg);
		}).play;
	}

	globalMessage {
		| t, msg |
		var command, parameterName, parameterValue, instrId, instrIdMatch;
		// should not be nil at this point, but if it is, it needs to be initialized.
		//if ( speechBufferPair == nil || speechBufferPair.size != 2, {
		//	speechBufferPair = this.sfManager.getRandomBufferPair;
		//});
		// assign the OSC parameters to values.
		/*
		msg[0] -> the application name (should be /ultimatum)
		msg[1] -> the source instrument should "global" in this method
		msg[2] -> the parameter name
		msg[3] -> the the parameter value
		*/
		if ( msg.size >= 2, {
			//var buffer, pan = 0.5;
			"Raw OSC message: % : %".format(msg,t).postln;
			/*
			NASTY HACK ALERT
			The Java com.illposed.osc library cannot send Java Integer values as a single parameter, as it just
			shows as garbage in the 'msg' array.
			Worse yet, if I try to wrap them in a string as the LAST parameter, I crash SCLang.
			Here, I am stuffing both the instrumentId and the challengeId in the first parameter as
			a string in the form instr1_2 (instrumentId = 1, challengeId = 2). Am using a regular expression
			to suck the numerical values from the string. I need to get this shit to work and don't have time
			to troubleshoot byte conversions in OSC messages.
			*/
			// The instrumentId is the first numerical value (1) in the string instr1_2
			// The challengeId is the second numerical value (2) in the string instr1_2
			parameterName = msg.at(1).asString;
			parameterValue = msg.at(2).asString;
			"Incoming message at %: %".format(t, msg).postln;
			//if ( this.class.varNames.includes( parameterName.asSymbol ), {
			if ( parameterName.matchRegexp( "performanceDuration" ), {
				"Setting Performance Duration to %".format(parameterValue).postln;
				this.performanceDuration = parameterValue.asFloat;
			});
			if ( parameterName.matchRegexp( "startPerformance" ), {
				this.startPerformance( parameterValue );
			});
			if ( parameterName.matchRegexp( "testPerformance" ), {
				this.testPerformance( parameterValue );
			});
		}, {
			"Wrong number of parameters in command % issues at %".format(msg, t).postln;
		});
	}

	/*
	recordBuffer is the internal method for trigger a buffer recording from AudioIn, i.e. it is not triggered by an OSC message.
	*/
	recordBuffer {
		/* arguments:
		sampleDur : the duration of the sample that will be recorded
		instrId : the instrument requesting the sample recording. Not used yet...
		*/
		| sampleDur = 8.0, instrId = 0 |
		var buffer, fileName;
		Task.new({
			var inputChannel = 1;
			buffer = Buffer.alloc(this.server, this.server.sampleRate * sampleDur.asFloat, 1);
			this.server.sync;
			// a four second 1 channel Buffer
			//this.server.sendMsg("/s_new", \recordBuffer, this.server.nextNodeID, 0, 0,
			//	buffer.bufnum, sampleDur.asFloat );
			inputChannel = [ 1, 2 ].choose;
			Synth(\recordBuffer, [ \input, inputChannel, \duration, sampleDur.asFloat, \buf, buffer.bufnum, \doneAction, 2 ]);
			~date = Date.getDate;
			fileName = "%/sample_%.wav".standardizePath.format(this.tempSfDir, ~date.stamp);
			"started recording to % at %".format(fileName, ~date).postln;
			(sampleDur.asFloat + 0.1).wait;
			this.isWritingToDisk = true;
			buffer.write( fileName ,
				headerFormat: "wav",
				sampleFormat: "int24",
				numFrames: -1,
				startFrame: 0,
				leaveOpen: false,
				completionMessage:{ "finished writing file % at %".format( fileName, ~d ) } );
			this.server.sync;
			0.5.wait;
			this.isWritingToDisk = false;
		}).play;
	}

	recordBufferOSC {
		|t, msg|
		var instrId, instrIdMatch, command, challengeId, challengeIdMatch,
		pair, buffer, fileName, sampleDur = 8.0;
		// should not be nil at this point, but if it is, it needs to be initialized.
		//if ( speechBufferPair == nil || speechBufferPair.size != 2, {
		//	speechBufferPair = this.sfManager.getRandomBufferPair;
		//});
		// assign the OSC parameters to values.
		/*
		msg[0] -> the application name (should be /ultimatum)
		msg[1] -> the source instrument (instrX where X -> 0..n)
		msg[2] -> the duration to record. Should not be more than 20 seconds
		The challenge id is used to pair the various voice commands together.
		*/
		"Raw OSC message: % : %".format(msg,t).postln;
		if ( msg.size >= 2, {
			//var buffer, pan = 0.5;
			"Raw OSC message has >=2 parameters: % : %".format(msg,t).postln;
			/*
			NASTY HACK ALERT
			The Java com.illposed.osc library cannot send Java Integer values as a single parameter, as it just
			shows as garbage in the 'msg' array.
			Worse yet, if I try to wrap them in a string as the LAST parameter, I crash SCLang.
			Here, I am stuffing both the instrumentId and the challengeId in the first parameter as
			a string in the form instr1_2 (instrumentId = 1, challengeId = 2). Am using a regular expression
			to suck the numerical values from the string. I need to get this shit to work and don't have time
			to troubleshoot byte conversions in OSC messages.
			*/
			// The instrumentId is the first numerical value (1) in the string instr1_2
			instrIdMatch = msg.at(1).asString.findAllRegexp(".*instr([0-9]+)_");
			// The challengeId is the second numerical value (2) in the string instr1_2
			challengeIdMatch = msg.at(1).asString.findAllRegexp(".*instr[0-9]+_([0-9]+)");
			fileName = "%/%-%.wav".format( this.tempSfDir, this.appId, t );
			fileName.postln;
			sampleDur = msg.at(2).asString;
			if ( sampleDur.asFloat.isPositive && sampleDur.asFloat < 12,
				{
				this.recordBuffer( sampleDur, instrId);
			}, {
					"unable to parse sample duration from %".format(msg.at(2).asString);
				}
			);
		});
	}

	/*
	playRecordedSamples is the internal method for playing the samples recorded from the live performance back through the processing synth channels.
	*/
	playRecordedSamples {
		| outputSynth, samplePlaybackBus, eventDuration = 20 |
		/* arguments:
		outputSynth : the synth that will use the enclosed Synths as input.
		samplePlaybackBus : the Bus that ~pvSynth will to write to.
		*/
		var buf1samp, buf2samp, outBuf, mainOutBus, mainEffectBus, pvOutBus, pvBus1, pvBus2, mainCompander, effect1;
		~sampleFiles = "%/*.wav".format( this.tempSfDir ).pathMatch;
		~file1 = ~sampleFiles.choose;
		~sampleFiles.remove( ~file1 );
		~file2 = ~sampleFiles.choose;
		/**
		Basic framework:
		c = ~samplePlaybackBus here
		x = Synth.before( v, \diffuser, [ \outBus, d, \in1, c, \in2, c, \xfadeRate, rand( 0.25, 1.0 ) ] );
		//x = Synth.new( \pv_morph, [ \outBus, b, \in1, c, \in2, c, \xfadeRate, rand( 0.1 ) ] );
		y = Synth.before( x, \sampleShifter, [ \output, c, \bufnum, buf1samp.bufnum, \amp, rrand( 0.75, 1.0 ),
			\scale, rrand( -1.0, 1.0 ),
			\attack, rrand( 0.001, 0.3), \duration, max( 2.0, buf1Dur / 2 ), \freq, 1, \env, envelope ]);
		z = Synth.before( x, \sampleShifter, [ \output, c, \bufnum, buf2samp.bufnum, \amp, rrand( 0.75, 1.0 ),
			\scale, rrand( -1.0, 1.0 ) * 1.0,
			\attack, rrand( 0.001, 0.3 ), \duration, max( 2.0, buf1Dur / 2 ), \freq, 1, \env, envelope ]);
		**/
		~buffer1 = Buffer.read(this.server, ~file1, startFrame: 0, numFrames: -1, action: 2);
		~buffer1.updateInfo;
		//this.server.sync;
		~buffer2 = Buffer.read(this.server, ~file2, startFrame: 0, numFrames: -1, action: 2);
		~buffer2.updateInfo;
		this.server.sync;
		~bufDur1 = ~buffer1.numFrames / ~buffer1.sampleRate;
		~bufDur2 = ~buffer2.numFrames / ~buffer2.sampleRate;

		"Selected sample file1 %s".format( ~buffer1.path ).postln;
		"Selected sample file2 %s".format( ~buffer2.path ).postln;

		// subtract tiny amount from the duration so that the doneAction will trigger
		// when the gate moves to 0.
		~sampDur = max( ~bufDur1, ~bufDur2 ) - 0.01;
		~synthDur = min( max( ~bufDur1, ~bufDur2 ) / 2, max( ~bufDur1, ~bufDur2 ) );
		outputSynth.set( \qVal, rrand( 1.5, 2.5 ) );
		outputSynth.set( \freq, rrand( 500, 2500 ) );

		~phi = 5.0.sqrt - 1 / 2;
		~levels = [ 0,1,0 ];
		~times = [ eventDuration * ~phi, eventDuration * (1 - ~phi) - 0.1 ];
		~pvEffectBus = Bus.audio(this.server, 2);
		~pvSynth = [
			Synth.before( outputSynth, \diffuser, [ \outBus, samplePlaybackBus, \in1, ~pvEffectBus, \in2, ~pvEffectBus, \xfadeRate, rand( 0.25, 1.0 ) ] ),
			Synth.before( outputSynth, \pv_morph, [ \outBus, samplePlaybackBus, \in1, ~pvEffectBus, \in2, pvBus2, \xfadeRate, rand( 0.2, 1.5 ) ] ),
			Synth.before( outputSynth, \rand_comb, [ \outBus, samplePlaybackBus, \in1, ~pvEffectBus, \in2, pvBus2, \combRate, rand( 0.2, 1.5 ), \envLevels, ~levels.reference, \envTimes, ~times.reference ] ),
			Synth.before( outputSynth, \xfade, [ \outBus, samplePlaybackBus, \in1, ~pvEffectBus, \in2, pvBus2, \combRate, rand( 0.2, 1.5 ) ] )
		].choose;


		//~envelope = Env.sine( min( ~bufDur1, ~bufDur2 ) - 0.01, rrand( 0.75, 1.0 ) );
		//levels, times,

		~sampleShifterSynth1 = Synth.before( ~pvSynth, \sampleShifter_new,
		[
			\output, ~pvEffectBus,
			\bufnum, ~buffer1.bufnum,
			\maxAmp, rrand( 0.5, 1.0 ),
			\scale, rrand( 0.35, 0.85 ) * 1.0,
			\attack, rrand( 0.001, 0.3),
			\duration, ~sampDur,
			\freq, 1,\envTimes, ~times.reference,
			\envLevels, ~levels.reference
		]);
		~sampleShifterSynth2 = Synth.before( ~pvSynth, \sampleShifter_new,
		[
			\output, ~pvEffectBus,
			\bufnum, ~buffer2.bufnum,
			\maxAmp, rrand( 0.5, 1.0),
			\scale, rrand( 0.35, 0.85 ) * 1.0,
			\attack, rrand( 0.001, 0.3),
			\duration, ~sampDur,
			\freq, 1,\envTimes, ~times.reference,
			\envLevels, ~levels.reference
		]);
		this.server.queryAllNodes.postln;

		// a short wait time between the file appearing in the directory and using it.
		0.5.wait;
		//[ ~file1, ~file2 ].postln;
	}

	playWav {
		| t, msg |
		var instrId, instrIdMatch, command, challengeId, challengeIdMatch, pair, buffer, pan = 0.5, bufferlength, state = -1,
		    challengedWasIssued = false, responseWasIssued = false, speechBufferPair;
		// should not be nil at this point, but if it is, it needs to be initialized.
		//if ( speechBufferPair == nil || speechBufferPair.size != 2, {
		//	speechBufferPair = this.sfManager.getRandomBufferPair;
		//});
		// assign the OSC parameters to values.
		/*
		msg[0] -> the application name (should be /ultimatum)
		msg[1] -> the source instrument (instrX where X -> 0..n)
		msg[2] -> the command, one of 'challenge', 'done', 'punt', 'finish'
		msg[3] -> the challenge id. A unique number assigned to each challenge unit (challenge, done, punt)
		The challenge id is used to pair the various voice commands together.
		*/
		if ( msg.size >= 2, {
			//var buffer, pan = 0.5;
			"Raw OSC message: % : %".format(msg,t).postln;
			/*
			NASTY HACK ALERT
			The Java com.illposed.osc library cannot send Java Integer values as a single parameter, as it just
			shows as garbage in the 'msg' array.
			Worse yet, if I try to wrap them in a string as the LAST parameter, I crash SCLang.
			Here, I am stuffing both the instrumentId and the challengeId in the first parameter as
			a string in the form instr1_2 (instrumentId = 1, challengeId = 2). Am using a regular expression
			to suck the numerical values from the string. I need to get this shit to work and don't have time
			to fuck around with byte conversions in OSC messages.
			*/
			// The instrumentId is the first numerical value (1) in the string instr1_2
			instrIdMatch = msg.at(1).asString.findAllRegexp(".*instr([0-9]+)_");
			// The challengeId is the second numerical value (2) in the string instr1_2
			challengeIdMatch = msg.at(1).asString.findAllRegexp(".*instr[0-9]+_([0-9]+)");
			command = msg.at(2).asString;
			if ( instrIdMatch.rank > 2 && challengeIdMatch.rank > 2,
				{
					instrId = instrIdMatch.at(0).at(1).at(1).asInteger;
					// select the Pan value based on the instrId.
					if ( instrId % 2 == 0, { pan = 1.0; }, { pan = -1.0; } );
					challengeId = challengeIdMatch.at(0).at(1).at(1).asInteger;
					"Incoming message at %: InstrumentId: %, ChallengeId: %, Command: %".format(t, instrId, challengeId, command).postln;
					if ( command == "challenge", {
						// For a new challenge, we should always generate a new speechBufferPair
						this.speechBufferPair = this.sfManager.getRandomBufferPair;
						buffer = this.speechBufferPair.at(0);
						"playing buffer % buffer id: %".format(buffer.path, buffer.bufnum).postln;
						//this.server.sendMsg("/s_new", \samplePlayer, this.server.nextNodeID, 0, 0,
						this.server.sendMsg("/s_new", \samplePlayer, this.server.nextNodeID, 0, 0,
							"outbus", this.outbus, "freqScale", rrand(0.9,1.1), "startPosition", 0,
							"duration", BufferHelper.getBufferDuration(buffer),
							"bufnum", buffer.bufnum, "amp", 1.00 , "scale", rrand( 1.0, 0.5 ),
							"attack",0.02, "numChannels", buffer.numChannels, "pan", pan );
					});
					if ( command == "done", {
						// for the response, the Pan value is reversed, since it is sent by
						// the challengee, not the challenger.
						if ( instrId % 2 == 1, { pan = 0.75; }, { pan = -0.75; } );
						if ( this.speechBufferPair != nil, {
							buffer = this.speechBufferPair.at(1);
							"playing buffer % buffer id: %".format(buffer.path, buffer.bufnum).postln;
							//this.server.sendMsg("/s_new", \samplePlayer, this.server.nextNodeID, 0, 0,
							this.server.sendMsg("/s_new", \samplePlayer, this.server.nextNodeID, 0, 0,
								"outbus", this.outbus, "freqScale", rrand(0.9,1.1), "startPosition", 0,
								"duration", BufferHelper.getBufferDuration(buffer),
								"bufnum", buffer.bufnum, "amp", 1.00 , "scale", rrand( 1.0, 0.5 ),
								"attack",0.002, "numChannels", buffer.numChannels, "pan", pan );
							}, {
							"Cannot get a sample to play, speechBufferPair in nil".postln;
						});
					});
					if ( command == "finish", {
						" % command triggered at %".format( command, t).postln;
						buffer = this.sfManager.getRandomFinishedBuffer;
						//this.server.sendMsg("/s_new", \samplePlayer, this.server.nextNodeID, 0, 1,
						this.server.sendMsg("/s_new", \samplePlayer, this.server.nextNodeID, 0, 0,
							"outbus", this.outbus, "freqScale", rrand(0.99,1.01), "startPosition", 0,
							"duration", BufferHelper.getBufferDuration(buffer),
							"bufnum", buffer.bufnum, "amp", 1.00 , "scale", rrand( 1.0, 0.5 ),
							"attack",0.002, "numChannels", buffer.numChannels, "pan", LFNoise1.kr( 1/10 ) );
					});
					if ( command == "punt", {
						this.punt(t, instrId, command);
					});
					if ( command == "puntAndResponse", {
						this.puntAndResponse(t, instrId, command);
					});
					if ( command == "randomSample", {
						this.punt(t, instrId, command);
					});
				},
				{
					"Unable to parse Instrument ID from OSC message: % at %".format(msg, t).postln;
			});
		}, {
			"Wrong number of parameters in command % issues at %".format(msg, t).postln;
		});
	}

	punt {
	|t,instr,cmd|
		var buffer, event, pan;
		"Instrument % issued a % command at %".format( instr, cmd, t ).postln;
		buffer = this.sfManager.getRandomSoundBuffer;
		buffer.path.postln;
		//file.openRead;
		//event = file.cue( (\firstFrame : 0, \addAction : 2, \server : this.server) );
		//this.server.sendMsg("/s_new", \samplePlayer, this.server.nextNodeID, 0, 1,
		this.server.sendMsg("/s_new", \samplePlayer, this.server.nextNodeID, 0, 0,
			"outbus", this.outbus, "freqScale", rrand(0.99,1.01), "startPosition", 0,
			"duration", BufferHelper.getBufferDuration(buffer),
			"bufnum", buffer.bufnum, "amp", 1.00 , "scale", rrand( 1.0, 0.5 ),
			"attack",0.002, "numChannels", buffer.numChannels, "pan", LFNoise1.kr( 1/5 ) );

		//event.play;
		//file.close;
	}

	/*
	The puntAndResponse method choses a random punt sample, plays it, waits for the duration of the
	punt sample, then plays the response voice sample.
	*/
	puntAndResponse {
	|t,instr,cmd|
		var buffer, responseBuffer, event, pan;
		"Instrument % issued a % command at %".format( instr, cmd, t ).postln;
		buffer = this.sfManager.getRandomSoundBuffer;
		buffer.path.postln;
		//file.openRead;
		//event = file.cue( (\firstFrame : 0, \addAction : 2, \server : this.server) );
		//this.server.sendMsg("/s_new", \samplePlayer, this.server.nextNodeID, 0, 1,
		if ( instr % 2 == 1, { pan = 1.0; }, { pan = -1.0; } );
		Routine.new({
			this.server.sendMsg("/s_new", \samplePlayer, this.server.nextNodeID, 0, 0,
				"outbus", this.outbus, "freqScale", rrand(0.99,1.01), "startPosition", 0,
				"duration", BufferHelper.getBufferDuration(buffer),
				"bufnum", buffer.bufnum, "amp", 1.00 , "scale", rrand( 1.0, 0.5 ),
				"attack",0.002, "numChannels", buffer.numChannels, "pan", LFNoise1.kr( 1/10 ) );
			BufferHelper.getBufferDuration(buffer).wait;
			responseBuffer = this.speechBufferPair.at(1);

			"playing buffer % buffer id: %".format(responseBuffer.path, responseBuffer.bufnum).postln;
			this.server.sendMsg("/s_new", \samplePlayer, this.server.nextNodeID, 0, 0,
				"outbus", this.outbus, "freqScale", rrand(0.9,1.1), "startPosition", 0,
				"duration", BufferHelper.getBufferDuration(responseBuffer),
				"bufnum", responseBuffer.bufnum, "amp", 1.00 , "scale", rrand( 1.0, 0.5 ),
				"attack",0.002, "numChannels", responseBuffer.numChannels, "pan", pan );
		}).play;
		//event.play;
		//file.close;
	}

	getPanValue {
		|panMin, panMax|

	}

	/*
	All application defaults are set here. If a required key does not exist in the appConfig Map,
	the default value is supplied here.
	*/
	setAppConfig {
		~defaultMinSampleRecordTrigger = 20.0;
		~defaultMaxSampleRecordTrigger = 45.0;
		~defaultSampleDuration = 10;
		~defaultOSCPort = 57120;
		~defaultAppId = "ultimatum";
	}
}