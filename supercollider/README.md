This is a README file for setting up the SuperCollider layer of the Ultimatium Game Composition.

The SuperCollider later of Ultimatum consists of two class files:
	supercollider/classes/SoundFileManager.sc
	supercollider/classes/UltimatumPlayer.sc

The application is controlled by  driver file:
	supercollider/ultimatum-app.sc

Application configuration values are specified in:
	supercollider/ultimatum.yaml
The ultimatum.yaml file must reside in the same directory as the ultimatum-app.sc file.
	
The structure of the Sound File directory as specified in the appDirectory parameter in ultimatum.yaml is:

sounds
speech
temp_sounds
staging

The contents of "sounds" can be augmented by the performers if desired. This directory contains the files chosen by
the "punt" commands in the performance.

The contents of "speech" must not be changed. This directory contains the speech samples.

The contents of "tempo_sounds" is deleted at the beginning of each performance. It contains the files recorded during
the live performance that are used as a background to the main performance.

The contents of "staging" is deleted at the beginning of each performance. It is the staging directory for the files
recorded during the live performance..

With the application directories and sounds files in place and the SuperCollider classes copied to the Class file
path, the SuperCollider layer can be started by running the ultimatum-app.sc file.

The application will listen on UDP port 57120 for OSC commands. This may be changed, but will require coordinating the 
Web Application layer as well.

