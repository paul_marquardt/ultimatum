package ultimatum
import java.util.Date;

import ultimatum.ChallengeManager
import static org.junit.Assert.*
import grails.test.*
import grails.test.mixin.*

import org.junit.*

import grails.test.mixin.*
import org.junit.*
import grails.util.Holders

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
class ChallengeServiceIntegrationTests extends GrailsUnitTestCase {

	PerformanceService performanceService
	PerformerService performerService
	def challengeService
	def newPerformance
	def perfA, perfB, perfC
	
	@Before
	void setUp() {
		performanceService = new PerformanceService()
		newPerformance = performanceService.createPerformance([ name : "Test ChallengeService", passcode : "123456", duration : 600 ] )
		assertNotNull newPerformance
		
		/*
		 * 
		 * 	String name
	String passcode
	Double duration
	Date startTime
	boolean isStarted = false
	PerformanceEnvironment performanceEnvironment

		 */
	}

	@After
	void tearDown() {
		// Tear down logic here
		newPerformance.delete()
	}

	/*
	@Test
    void testSendChallenge() {
		def messageType = "challenge"
		def messageBody = "Challenge: ${perfA.name} challenged ${perfB.name}"
		challengeService.sendChallenge(perfA, perfB, messageType, messageBody)
		messageBody = "Challenge: ${perfB.name} challenged ${perfA.name}"
		challengeService.sendChallenge(perfB, perfA, messageType, messageBody)
	}
	*/
	
	@Test
	void testSendChallenge() {
		performerService = new PerformerService()
		perfA = performerService.createPerformer("Jerry", "pa55code", "center", 1, newPerformance)
		assertNotNull perfA
		assertEquals perfA.class, Performer.class
		assertNotNull perfA.id
		perfB = performerService.createPerformer("Karen", "pa55code", "left", 1, newPerformance)
		assertNotNull perfB
		assertEquals perfB.class, Performer.class
		assertNotNull perfB.id
		/*
		perfC = performerService.createPerformer("Chris", "pa55code", "left", 1, newPerformance)
		assertNotNull perfC.save()
		assertEquals perfC.class, Performer.class
		assertNotNull perfC.id
		*/
		challengeService = new ChallengeService()
		log.info( "${perfA} :: ${perfB}")
		def messageType = "challenge"
		def messageBody = "Challenge: ${perfA.name} challenged ${perfB.name}"
		def msg = challengeService.sendChallenge(perfA.id, perfB.id, messageType, messageBody)
		log.info( msg )
		def myChallenges = challengeService.getChallengesFrom(perfA.id)
		assertNotNull myChallenges
		assert myChallenges.size() > 0
		for ( Challenge chall : myChallenges ) {
			assert chall instanceof Challenge
		}
		myChallenges = challengeService.getChallengesTo(perfB.id)
		assertNotNull myChallenges
		assert myChallenges.size() > 0
		for ( Challenge chall : myChallenges ) {
			assert chall instanceof Challenge
		}

		messageBody = "Challenge: ${perfB.name} challenged ${perfA.name}"
		msg = challengeService.sendChallenge(perfB.id, perfA.id, messageType, messageBody)
		log.info( msg )
		myChallenges = challengeService.getChallengesFrom(perfB.id)
		assertNotNull myChallenges
		assert myChallenges.size() > 0
		for ( Challenge chall : myChallenges ) {
			assert chall instanceof Challenge
		}

		myChallenges = challengeService.getChallengesTo(perfA.id)
		assertNotNull myChallenges
		assert myChallenges.size() > 0
		for ( Challenge chall : myChallenges ) {
			assert chall instanceof Challenge
		}
	}

	@Test
	void testSetAcknowledged() {
		
		performerService = new PerformerService()
		performanceService = new PerformanceService()
		challengeService = new ChallengeService()
		//newPerformance = performanceService.createPerformance("Test ChallengeService", "123456", 600)
		assertNotNull newPerformance.save()
		perfA = performerService.createPerformer("Jerry", "pa55code", "center", 1, newPerformance)
		assertNotNull perfA
		assertEquals perfA.class, Performer.class
		assertNotNull perfA.id
		perfB = performerService.createPerformer("Karen", "pa55code", "left", 1, newPerformance)
		assertNotNull perfB
		assertEquals perfB.class, Performer.class
		assertNotNull perfB.id
		/*
		perfC = performerService.createPerformer("Chris", "pa55code", "left", 1, newPerformance)
		assertNotNull perfC.save()
		assertEquals perfC.class, Performer.class
		assertNotNull perfC.id
		*/
		log.info( "${perfA} :: ${perfB}")
		def messageType = "challenge"
		def messageBody = "Challenge: ${perfA.name} challenged ${perfB.name}"
		challengeService.sendChallenge(perfA.id, perfB.id, messageType, messageBody)
		// Setup logic here
		def myChallenges = challengeService.getChallengesTo(perfB.id)
		assertNotNull myChallenges
		assert myChallenges.size() > 0

		for ( Challenge chall : myChallenges ) {
			challengeService.setAcknowledged(chall.id)
		}
		for ( Challenge chall : myChallenges ) {
			assertTrue( chall.acknowledged )
		}

	}

	@Test
	void testSetResponded() {
		
		performerService = new PerformerService()
		performanceService = new PerformanceService()
		challengeService = new ChallengeService()
		//newPerformance = performanceService.createPerformance("Test ChallengeService", "123456", 600)
		assertNotNull newPerformance.save()
		perfA = performerService.createPerformer("Jerry", "pa55code", "center", 1, newPerformance)
		assertNotNull perfA
		assertEquals perfA.class, Performer.class
		assertNotNull perfA.id
		perfB = performerService.createPerformer("Karen", "pa55code", "left", 1, newPerformance)
		assertNotNull perfB
		assertEquals perfB.class, Performer.class
		assertNotNull perfB.id
		/*
		perfC = performerService.createPerformer("Chris", "pa55code", "left", 1, newPerformance)
		assertNotNull perfC.save()
		assertEquals perfC.class, Performer.class
		assertNotNull perfC.id
		*/
		log.info( "${perfA} :: ${perfB}")
		def messageType = "challenge"
		def messageBody = "Challenge: ${perfA.name} challenged ${perfB.name}"
		challengeService.sendChallenge(perfA.id, perfB.id, messageType, messageBody)
		// Setup logic here
		def myChallenges = challengeService.getChallengesTo(perfB.id)
		assertNotNull myChallenges
		assert myChallenges.size() > 0

		for ( Challenge chall : myChallenges ) {
			challengeService.setResponded(chall.id)
		}
	}
}
