package ultimatum

import static org.junit.Assert.*
import grails.test.*
import grails.test.mixin.*
import org.junit.*
import grails.test.mixin.*
import org.junit.*
import ultimatum.PerformerService;
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsParameterMap

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
class PerformerServiceIntegrationTests extends GrailsUnitTestCase {

	def PerformerService
	def PerformanceService
	def Performance newPerformance
	@Before
	void setUp() {
		performerService = new PerformerService()
		performanceService = new PerformanceService()
		newPerformance = performanceService.createPerformance( [name : "Test Save Performer", passcode : "9999", duration : 600 ])
		//newPerformance = new Performance(name : "Test Save Performer", passcode : "9999", duration : 600, startTime : null)
		assertNotNull(newPerformance)
		// Setup logic here
	}

	@After
	void tearDown() {
		// Tear down logic here
		
	}

	
	@Test
    void testSavePerformer() {
		def savedPerformer = performerService.createPerformer("joe", "pa55code", "center", 1, newPerformance)
	   	assertNotNull savedPerformer.save()
		assertEquals savedPerformer.class, Performer.class
		assertNotNull savedPerformer.id
		def foundPerformer = Performer.get(savedPerformer.id)
		//foundUser.delete()
		assertNotNull foundPerformer
		foundPerformer.delete()
		//performerService.deletePerformer(foundPerformer)
		assertFalse Performer.exists(foundPerformer.id)
    }

	@Test
    void testSaveWithParams() {
		def params = [ name : "joe", passcode : "pa55code", position : "left", audioInput : 1]
		def paramMap = new GrailsParameterMap(params, null)
		def savedPerformer = performerService.createPerformer(paramMap, newPerformance)
	   	assertNotNull savedPerformer.save()
		assertEquals savedPerformer.class, Performer.class
		assertNotNull savedPerformer.id
		def foundPerformer = Performer.get(savedPerformer.id)
		//foundUser.delete()
		assertNotNull foundPerformer
		foundPerformer.delete()
		//performerService.deletePerformer(foundPerformer)
		assertFalse Performer.exists(foundPerformer.id)
    }

	@Test
    void testSaveWithNullPasscode() {
		def params = [ name : "larry", position : "right", audioInput : 1]
		def paramMap = new GrailsParameterMap(params, null)
		def savedPerformer = performerService.createPerformer(paramMap, newPerformance)
		assertTrue savedPerformer.validate()
	   	assertNotNull savedPerformer.save()
		assertEquals savedPerformer.class, Performer.class
		assertNotNull savedPerformer.id
		def foundPerformer = Performer.get(savedPerformer.id)
		//foundUser.delete()
		assertNotNull foundPerformer
		foundPerformer.delete()
		//performerService.deletePerformer(foundPerformer)
		assertFalse Performer.exists(foundPerformer.id)
    }

	void testSaveThenDelete() {
		def savedPerformer = performerService.createPerformer("linda", "pa55code", "center", 1, newPerformance)
		assertTrue savedPerformer.validate()
		assertNotNull savedPerformer.save()
		//assertNotNull newUser.save()
		assertNotNull savedPerformer
		def foundPerformer = Performer.get(savedPerformer.id)
		//foundUser.delete()
		assertNotNull foundPerformer
		foundPerformer.delete()
		performerService.deletePerformer(foundPerformer.id)
		assertFalse Performer.exists(foundPerformer.id)
	}
	
	@Test
	void testValidateLogin() {
		def params = [ name : "larry", performerPasscode : "12345", position : "right", audioInput : 1]
		def paramMap = new GrailsParameterMap(params, null)
		def savedPerformer = performerService.createPerformer(paramMap, newPerformance)
		assertTrue savedPerformer.validate()
		assertNotNull savedPerformer.save()
		assertEquals savedPerformer.class, Performer.class
		assertNotNull savedPerformer.id
		def foundPerformer = Performer.get(savedPerformer.id)
		// test validateLogin with valid performer passcode and null performance passcode
		def validatedPerformer = performerService.validateLogin("larry", "12345", null)
		assertNotNull(validatedPerformer)
		assertEquals validatedPerformer.class, Performer.class

		// test validateLogin with valid performer passcode and valid performance passcode
		validatedPerformer = performerService.validateLogin("larry", "12345", "9999")
		assertNotNull(validatedPerformer)
		assertEquals validatedPerformer.class, Performer.class

		// test validateLogin with valid performer  and invalid performer passcode
		assertNull( performerService.validateLogin("larry", "54321", "9999") )
		
		validatedPerformer.delete()
		//performerService.deletePerformer(foundPerformer)
		assertFalse Performer.exists(validatedPerformer.id)

	}
}
