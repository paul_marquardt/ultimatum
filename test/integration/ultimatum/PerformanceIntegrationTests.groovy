package ultimatum


import static org.junit.Assert.*
import grails.test.*
import grails.test.mixin.*
import org.junit.*
import grails.util.Holders

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
class PerformanceIntegrationTests extends GrailsUnitTestCase {

	String performanceName = "Test Performance"
	def performanceEnvironment
	
	@Before
	void setUp() {
		performanceEnvironment = new PerformanceEnvironment(
			oscRemoteHost : Holders.config.ultimatum.osc.remoteHost,
			oscRemotePort : Holders.config.ultimatum.osc.remotePort,
			applicationId : Holders.config.ultimatum.osc.applicationId
		)

		// Setup logic here
	}

	@After
	void tearDown() {
		// Tear down logic here
	}

	
	@Test
    void testSavePerformance() {
		Double perfDuration = 600.0
		def savedPerformance = new Performance (name : "Save Performance", passcode : null, duration : perfDuration, startTime : new Date(), performanceEnvironment : performanceEnvironment )
		assertTrue savedPerformance.validate()
		savedPerformance.save()
	   	assertNotNull savedPerformance
		assertEquals savedPerformance.class, Performance.class
		assertFalse savedPerformance.isStarted
		savedPerformance.isStarted = true
		assertTrue savedPerformance.isStarted
		def foundPerformance = Performance.findById(savedPerformance.id)
		assertNotNull foundPerformance
		assertTrue foundPerformance.isStarted
		savedPerformance.delete()
    }

	@Test
	void testSaveAndDeletePerformance() {
		Double perfDuration = 600.0
		def savedPerformance = new Performance (name : "Save and Delete Performance", passcode : null, duration : perfDuration, startTime : new Date(), performanceEnvironment : performanceEnvironment )
		assertTrue savedPerformance.validate()
		savedPerformance.save()
		assertNotNull savedPerformance
		assertEquals savedPerformance.class, Performance.class
		// add some performers
		def performer1 = new Performer(name : "linda", position : "left", audioInput : 2, performance : savedPerformance)
		assertNotNull performer1
		savedPerformance.addToPerformers(performer1)
		def performer2 = new Performer(name : "jorge", position : "left", audioInput : 2, performance : savedPerformance)
		assertNotNull performer2
		savedPerformance.addToPerformers(performer2)
		def performer3 = new Performer(name : "tracy", position : "right", audioInput : 3, performance : savedPerformance)
		assertNotNull performer3
		savedPerformance.addToPerformers(performer3)
		assertEquals 3, savedPerformance.performers.size()
		// delete the performance
		savedPerformance.delete(flush : true)
		assertEquals 0, Performance.count()
		// then verify that all performers were deleted through cascade.	
		assertEquals 0, Performer.count()
		//assertNull foundPerformance
		assertEquals Performance.list().size(), 0
	}

	@Test
	void testAddPerformers() {
		Double perfDuration = 600.0
		def savedPerformance = new Performance(name : "Add Performers Performance", passcode : null, duration : perfDuration, startTime : new Date(), performanceEnvironment : performanceEnvironment )
		assertTrue savedPerformance.validate()
		assertNotNull savedPerformance.save()
		assertEquals savedPerformance.class, Performance.class
		def performer1 = new Performer(name : "linda", position : "left", audioInput : 2)
		assertNotNull performer1
		savedPerformance.addToPerformers(performer1)
		def performer2 = new Performer(name : "jorge", position : "left", audioInput : 2)
		assertNotNull performer2
		savedPerformance.addToPerformers(performer2)
		def performer3 = new Performer(name : "tracy", position : "right", audioInput : 3)
		assertNotNull performer3
		savedPerformance.addToPerformers(performer3)
		assertEquals 3, savedPerformance.performers.size()		
	}


}
