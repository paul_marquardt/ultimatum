package ultimatum


import static org.junit.Assert.*
import grails.test.*
import grails.test.mixin.*
import org.junit.*
import grails.util.Holders

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
class OSCManagerIntegrationTests extends GrailsUnitTestCase {

	String performanceName = "Test Performance"
	
	@Before
	void setUp() {
		// Setup logic here
	}

	@After
	void tearDown() {
		// Tear down logic here
	}

	
	@Test
    void testOSCManagerFromParams() {
		Double perfDuration = 600.0
		def savedPerformance = new Performance (name : "OSCManager Test Performance", passcode : null, duration : perfDuration, startTime : new Date(), performanceEnvironment : null )
		assertTrue savedPerformance.validate()
		savedPerformance.save()
	   	assertNotNull savedPerformance
		assertEquals savedPerformance.class, Performance.class
		assertFalse savedPerformance.isStarted
		savedPerformance.isStarted = true
		savedPerformance.save()
		assertTrue savedPerformance.isStarted
		OSCManager oscMan = new OSCManager(
			Holders.config.ultimatum.osc.remoteHost,
			Holders.config.ultimatum.osc.remotePort,
			Holders.config.ultimatum.osc.applicationId
		)
		def sleepTime = 10000L
		oscMan.sendGlobalMessage( "startPerformance", savedPerformance.getName() )
		Thread.currentThread().sleep( sleepTime )
		oscMan.sendGlobalMessage( "performanceDuration", 1200)
		Thread.currentThread().sleep( sleepTime )
		oscMan.sendGlobalMessage( "performanceDuration", 1200)
		def challengeId = 1
		def perf1Id = 1
		def perf2Id = 2
		// send a normal Challenge/Response pair
		// send Challenge
		oscMan.sendChallengeMessage(perf1Id, challengeId)
		Thread.currentThread().sleep( sleepTime )
		// send a finish message from the other performer
		oscMan.sendFinishedMessage(perf2Id, challengeId)
		// increment challengeId for new round
		challengeId++
		oscMan.sendChallengeMessage(perf1Id, challengeId)
		Thread.currentThread().sleep( sleepTime )
		oscMan.sendPuntAndResponse(perf2Id, challengeId)
		Thread.currentThread().sleep( sleepTime )
		challengeId++
		oscMan.sendChallengeMessage(perf2Id, challengeId)
		Thread.currentThread().sleep( sleepTime )
		oscMan.sendFinishedMessage(perf1Id, challengeId)
		Thread.currentThread().sleep( sleepTime )
		oscMan.sendPuntMessage(perf1Id, challengeId)
		Thread.currentThread().sleep( sleepTime )
		oscMan.sendClosingMessage()

    }

	@Test
    void testOSCManagerPerformanceEnvironment() {
		Double perfDuration = 600.0
		def savedPerformance = new Performance (name : "Save Performance", passcode : null, duration : perfDuration, startTime : new Date() )
		assertTrue savedPerformance.validate()
		savedPerformance.save()
	   	assertNotNull savedPerformance
		assertEquals savedPerformance.class, Performance.class
		assertFalse savedPerformance.isStarted
		savedPerformance.isStarted = true
		savedPerformance.save()
		assertTrue savedPerformance.isStarted
		def pEnv = new PerformanceEnvironment(
			oscRemoteHost : Holders.config.ultimatum.osc.remoteHost,
			oscRemotePort : Holders.config.ultimatum.osc.remotePort,
			applicationId : Holders.config.ultimatum.osc.applicationId
		)
		OSCManager oscMan = new OSCManager( pEnv )
		def challengeId = 1
		def perf1Id = 1
		def perf2Id = 2
		// send a normal Challenge/Response pair
		// send Challenge
		oscMan.sendChallengeMessage(perf1Id, challengeId)
		def sleepTime = 5000L
		Thread.currentThread().sleep( sleepTime )
		// send a finish message from the other performer
		oscMan.sendFinishedMessage(perf2Id, challengeId)
		// increment challengeId for new round
		challengeId++
		oscMan.sendChallengeMessage(perf1Id, challengeId)
		Thread.currentThread().sleep( sleepTime )
		oscMan.sendPuntAndResponse(perf2Id, challengeId)
		Thread.currentThread().sleep( sleepTime )
    }


}
