package ultimatum

import static org.junit.Assert.*
import grails.test.mixin.*
import grails.test.mixin.support.*

import org.junit.*
import ultimatum.ChallengeManager

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */

@TestFor(ChallengeManager)
class ChallengeManagerTests {

	PerformerService performerService
	String sourcePerformerName = "joe"
	String targetPerformerName = "linda"

	@Before
	void setUp() {
		performerService = new PerformerService()
		// Setup logic here
	}

	@After
	void tearDown() {
		// Tear down logic here
	}

	@Test
    void testChallengeManager() {
		// create a Performer with a variety of values
		def newPerformance = new Performance(name : "Test Create Performer", passcode : null, duration : 600, startTime : null)
		assertNotNull newPerformance
		def performerA = new Performer( name : "Charles", position : "center", audioInput : 1, performance : newPerformance)
		def performerB = new Performer( name : "Betsy", position : "left", audioInput : 1, performance : newPerformance)
		def performerC = new Performer( name : "Janet", position : "right", audioInput : 1, performance : newPerformance)
	   	assertNotNull performerA
		assertNotNull performerB
		assertNotNull performerC
				
		def manager = new ChallengeManager()
		def type = "challenge"
		def message = "${performerA.name} challenges ${performerB.name}"
		manager.sendChallenge( performerA, performerB, type, message)
		message = "${performerB.name} challenges ${performerA.name}"
		manager.sendChallenge( performerB, performerA, type, message)
		
		message = "${performerA.name} and ${performerB.name} challenging ${performerC.name}"
		manager.sendChallenge( performerA, performerB, type, message)
		manager.sendChallenge( performerB, performerA, type, message)

		def challenges = manager.getChallenges(performerA)
		// performer.save()
		// create Performer with minimum values
    }

}
