package ultimatum



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Challenge)
class ChallengeTests {

	def newPerformance
	def sourcePerformer
	def targetPerformer
	
	@Before
	void setUp() {
		newPerformance = new Performance( name : "First Improv", passcode : "somepa55", duration : 600)
		sourcePerformer = new Performer( name : "John", position : "center", audioInput : 1, performance : newPerformance)
		targetPerformer = new Performer( name : "Linda", position : "center", audioInput : 1, performance : newPerformance)
		
	}

    void testCreateChallenge() {
		assertNotNull newPerformance
		assertNotNull sourcePerformer
		assertNotNull targetPerformer	
		def challenge = new Challenge( source: sourcePerformer, target : targetPerformer, type : "challenge", message : "A challenge")
		assertNotNull challenge
		assertEquals sourcePerformer.name, "John"
		assertEquals targetPerformer.name, "Linda"
		assertEquals challenge.type, "challenge"
		assertEquals challenge.message, "A challenge"
		
    }
}
