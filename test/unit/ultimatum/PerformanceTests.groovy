package ultimatum



import grails.test.mixin.*

import org.junit.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Performance)
class PerformanceTests {

	PerformerService performerService
	PerformanceService performanceService
	String performanceName = "Test Performance"

	@Before
	void setUp() {
		performerService = new PerformerService()
		performanceService = new PerformanceService()
		// Setup logic here
	}

	@After
	void tearDown() {
		// Tear down logic here
	}

	
	@Test
    void testSavePerformance() {
		Double perfDuration = 600.0
		def newPerformance = new Performance(name : performanceName, passcode : null, duration : perfDuration, startTime : null)
	   	assertNotNull newPerformance
		assertEquals newPerformance.class, Performance.class
		assertFalse newPerformance.isStarted
		newPerformance.isStarted = true
		assertTrue newPerformance.isStarted
    }

	@Test
	void testSaveAndDeletePerformance() {
		Double perfDuration = 600.0
		def newPerformance = new Performance(name : performanceName, passcode : null, duration : perfDuration, startTime : null)
		   assertNotNull newPerformance
		assertEquals newPerformance.class, Performance.class
		assertFalse newPerformance.isStarted
		newPerformance.isStarted = true
		assertTrue newPerformance.isStarted
	}

}
