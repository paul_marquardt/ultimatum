package ultimatum

import grails.test.mixin.*
import org.junit.*
import ultimatum.PerformerService;

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Performer)
class PerformerTests {

	PerformerService performerService
	String sourcePerformerName = "joe"
	String targetPerformerName = "linda"

	@Before
	void setUp() {
		performerService = new PerformerService()
		// Setup logic here
	}

	@After
	void tearDown() {
		// Tear down logic here
	}

	
	@Test
    void testCreatePerformer() {
		// create a Performer with a variety of values
		def newPerformance = new Performance(name : "Test Create Performer", passcode : null, duration : 600, startTime : null)
		def newPerformer = new Performer( name : "Joe", position : "center", audioInput : 1, performance : newPerformance)
	   	assertNotNull newPerformer
		assertEquals newPerformer.class, Performer.class
		// performer.save()
		// create Performer with minimum values
		newPerformer = new Performer( name : "Joe")
		assertNotNull newPerformer
		assertEquals newPerformer.class, Performer.class
    }

}
