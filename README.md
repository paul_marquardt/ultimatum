# README #

### What is this repository for? ###

* Ultimatum is a web application that runs the Ultimatum Interactive Game Composition
   for two improvising performers and computer playback.
* 2.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Required software: Grails 2.4.3, Tomcat 8.x, SuperCollider
* Ultimatum
* Configuration
* Dependencies
* There is no long-term persistence. Each time the application runs, it starts with a new
   data set, as it is geared towards live performance.
* SuperCOllider setup instructions can be found in the README file in the supercollider directory.
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Please contact marqrdt at {x: x>f,x<h}mail.com