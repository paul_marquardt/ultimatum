// Place your Spring DSL code here

import org.springframework.jms.core.JmsTemplate

import ultimatum.ChallengeManager
import ultimatum.PerformanceEnvironment

beans = {

	jmsConnectionFactory(org.apache.activemq.ActiveMQConnectionFactory) {
	  brokerURL = 'tcp://localhost:61616'
	}
	
	performanceEnvironment(PerformanceEnvironment) {
		oscRemoteHost = "localhost"
		oscRemotePort = 57120
		applicationId = "ultimatum"
	}
}