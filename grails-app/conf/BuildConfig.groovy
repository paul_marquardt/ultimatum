grails.servlet.version = "2.5" // Change depending on target container compliance (2.5 or 3.0)
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.target.level = 1.7
grails.project.source.level = 1.7
//grails.project.war.file = "target/${appName}-${appVersion}.war"

// uncomment (and adjust settings) to fork the JVM to isolate classpaths
//grails.project.fork = [
//   run: [maxMemory:1024, minMemory:64, debug:false, maxPerm:256]
//]

grails.project.dependency.resolver = "maven" // or ivy

grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // specify dependency exclusions here; for example, uncomment this to disable ehcache:
        // excludes 'ehcache'
    }
    log "warn" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    checksums true // Whether to verify checksums on resolve
    legacyResolve false // whether to do a secondary resolve on plugin installation, not advised and here for backwards compatibility

    repositories {
        inherits true // Whether to inherit repository definitions from plugins

        grailsPlugins()
        grailsHome()
        grailsCentral()

        mavenLocal()
        mavenCentral()

        // uncomment these (or add new ones) to enable remote dependency resolution from public Maven repositories
        //mavenRepo "http://snapshots.repository.codehaus.org"
        //mavenRepo "http://repository.codehaus.org"
        //mavenRepo "http://download.java.net/maven/2/"
		mavenRepo "https://repo1.maven.org/maven2/"
        mavenRepo "http://repository.jboss.com/maven2/"
		mavenRepo "https://oss.sonatype.org/content/repositories/snapshots/"
	}

    dependencies {
        // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes e.g.
		compile('org.apache.activemq:activemq-core:5.7.0')	
		runtime 'mysql:mysql-connector-java:5.1.22'
		// Include IllPosed OSC libraries in classpath
		compile "com.illposed.osc:javaosc-core:0.3"
		compile "de.sciss:netutil:1.0.0"
		test "org.spockframework:spock-grails-support:0.7-groovy-2.0"
    }

    plugins {
        //runtime ":hibernate:$grailsVersion"
		// for Grails 2.4, use specific versions
		runtime ":hibernate:3.6.10.18"
		
		// These appear not to be needed in Grails 2.4
		//runtime ":gsp-resources:0.4.4"
        //runtime ":resources:1.2.8"
		
		// Grails 2.4 has migrated from resources to assets-pipeline
		compile ":asset-pipeline:1.9.4"
		runtime ":twitter-bootstrap:3.3.1"
		
		// Breaks shit:
		//compile ":less-asset-pipeline:2.0.8"
		
		runtime ":jquery:1.11.1"

        build ":tomcat:7.0.39"
        runtime ":database-migration:1.4.0"
        compile ':cache:1.1.8'
		compile ":quartz:1.0.2"
		//compile ':background-thread:1.6'
		compile ":executor:0.3"
		compile ':jms:1.3'
		//compile ":events-push:1.0.M7"
		
		test(":spock:0.7") {
			exclude "spock-grails-support"
		}
    }

	grails.servlet.version = "3.0"
	grails.tomcat.nio = true
}
