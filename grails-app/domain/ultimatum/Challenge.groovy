package ultimatum

class Challenge implements Serializable {

	Performer source
	Performer target
	String type
	String message
	String gguid = UUID.randomUUID().toString()
	Date timestamp = new Date()
	boolean responded = false
	boolean acknowledged = false
	
	//static belongsTo = [ performance : Performance ]
    static constraints = {
		
    }
	
	public String toJSON() {
		def builder = new groovy.json.JsonBuilder()
		builder source : source.name, target : target.name, type : type, message : message, gguid : gguid, timestamp : timestamp
		return builder.toString()
	}
}
