package ultimatum

import java.io.Serializable;

//import ultimatum.PerformanceEnvironment

class Performance implements Serializable {

	String name
	String passcode
	Double duration
	Date startTime
	boolean isStarted = false
	Double panMin = -0.75
	Double panMax = 0.75
	PerformanceEnvironment performanceEnvironment
	
	static embedded = [ 'performanceEnvironment' ]
	static hasMany = [ performers : Performer, challenges : Challenge ]
	static transients = ['isStarted']
	
    static constraints = {
		passcode(nullable : true)
		startTime(nullable : true)
		isStarted(nullable : true)
    }
}

class PerformanceEnvironment {
	
	String oscRemoteHost
	int oscRemotePort
	String applicationId		
}
	
