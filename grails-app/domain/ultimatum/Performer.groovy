package ultimatum

//import java.util.Queue

class Performer implements Serializable {

	String name
	String passcode
	String position
	Integer audioInput
	Double panPosition
	String role
	//Queue<Challenge> challengeQueue
	// role will be "leader" or "player"
	// leaders will be able to control the performance (create/start/stop)
	// and add other players.
	
	static belongsTo = [ performance : Performance]
	
	//static transients = [ 'challengeQueue' ]
	
    static constraints = {
		//challengeQueue( nullable : true )
		name(unique : true, blank : false)
		position( nullable : true )
		panPosition( nullable : true )
		passcode( nullable : true )
		audioInput( nullable : true, min : 0, max : 20 )
    }
}
