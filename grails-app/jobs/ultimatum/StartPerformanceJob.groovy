package ultimatum
import org.quartz.JobExecutionContext

class StartPerformanceJob {

	def group = "AudioEventGroup"
    def execute() {
		def performanceService = new PerformanceService()
        // execute job
		/*
		OSCManager oscManager = new OSCManager( context.mergedJobDataMap.get('oscRemoteHost'),
			context.mergedJobDataMap.get('oscRemotePort'),
			context.mergedJobDataMap.get('applicationId')
		)
		*/
		def performance = performanceService.getPerformance()
		def pe = performance.performanceEnvironment
		OSCManager oscManager = new OSCManager( pe.oscRemoteHost,
			pe.oscRemotePort, pe.applicationId )
		print "Sending global message startPerformance with parameter ${performance.getName()}"
		//OSCManager oscManager = new OSCManager( 'localhost', 57120, 'ultimatum')
		/*
			Set the performanceduration on the SuperCollider side before starting it. This is essential
			to allow SC to schedule events in the synth engine layer.
		*/
		oscManager.sendGlobalMessage( "performanceDuration", performance.getDuration() )
		/*
		 Send the startPerformance message to the SuperCollider side. This method schedules events in the synth engine layer.
		*/
		oscManager.sendGlobalMessage( "startPerformance", performance.getName() )
    }
}
