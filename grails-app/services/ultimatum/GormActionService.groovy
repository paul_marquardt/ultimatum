package ultimatum

import org.slf4j.Logger;

class GormActionService {
	
	def saveAction( Object domainObject ) {
		log.info("saving ${domainObject}")
		try {
			domainObject.save()
			return domainObject
		} catch(e) {
			def errorString =  e.cause
			log.error("unable to save domainObject ${domainObject}: ${errorString}")
		}
	}
	
	def deleteAction( Object domainObject ) {
		log.info("deleting ${domainObject}")
		try {
			domainObject.delete()
			return domainObject
		} catch(e) {
			def errorString =  e.cause
			log.error("unable to delete domainObject ${domainObject}: ${errorString}")
		}
	}

}
