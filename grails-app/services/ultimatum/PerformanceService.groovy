package ultimatum

import javax.servlet.ServletContext;
import grails.util.Holders

class PerformanceService {
	
	def OSCManager
	Performance createPerformance( Map params ) {
		// if a performance already exists, delete it. This should guarantee only one Performance exists
		log.info("Calling: createPerformance(String name, String passcode, String durationString)")
		def performance = getPerformance()
		if ( performance != null ) {
			return performance
		}
		def pe = new PerformanceEnvironment(
			oscRemoteHost : Holders.config.ultimatum.osc.remoteHost,
			oscRemotePort : Holders.config.ultimatum.osc.remotePort,
			applicationId : Holders.config.ultimatum.osc.applicationId
		)
		// performance = new Performance(name : name, passcode : passcode, duration : duration, startTime : startTime, performanceEnvironment : pe )
		//performance = new Performance(name : name, passcode : passcode, duration : duration, startTime : startTime )
		performance = new Performance(params)
		if ( performance.validate() ) {
			Performance.withTransaction {
				//performance.lock()
				performance.save()
				log.info("Performance with name ${performance.name} and id ${performance.id} was saved.")
			}
		} else {
			performance.getErrors().each { error ->
				log.error("Error: ${error}")
			}
		}
		def performances = Performance.list()
		log.info("Listing all performances:")
		performances.each { perf ->
			log.info("\t${perf.name} :: ${perf.id}")
		}
		return performance
	}

    Performance createPerformance(String name, String passcode, Double duration) {
		// if a performance already exists, delete it. This should guarantee only one Performance exists
		//performanceEnvironment.save()
		def performance = createPerformance(name, passcode, duration, new Date() )
		return performance
    }

	void deletePerformance() {
		try {
			//def performance = getPerformance()
			// delete all PerformanceEnvironments
			def performances = Performance.list()
			if ( ! performances.empty ) {
				performances.each { perf ->
					Performance.withTransaction {
						perf.lock()
						perf.delete(flush : true)
					}
				}
			}
		} catch(e) {
			def errorString =  e.cause
			log.error("unable to delete Performance: ${errorString}")
		}
	}
	
	Performance getPerformance() {
		try {
			if ( Performance.list().size() == 0 ) { 
				return null
			}
			def performances = Performance.list()
			if ( performances.size() == 0 ) {
				return null
			}
			//Performance performance = Performance.get(performances[0])
			//return performance
			return performances[0]
		} catch(e) {
			def errorString =  e.cause
			log.error("unable to retrieve active Performance: ${errorString}")
		}
	}

	void startPerformance( Performance performance) {
		try {
			def quartzScheduler
			// set performance.startTime to now
			performance.startTime = new Date()
			// set started flag to true
			performance.isStarted = true
			performance.save()
			//def numberOfPerformers = performance.performers.size()
			def panRange = Math.abs( performance.panMax - performance.panMin )
			def index = 0
			// set the Pan position for each performer from left to right. Eventually, the performer will
			// be abkle to set their own pan position from the interface. For now, just set from left to right
			performance.performers.each {
				performer ->
				performer.panPosition = performance.panMin + ( (performer.id - 1) * panRange / (performance.performers.size() - 1) )
				performer.save()
			}
			log.info("Performance ${performance.name} was started.")
			// Golden Ratio
			def phi = 0.618034
			// closing timestamp is current timestamp + (phi * performance duration)
			def closingTimestamp = java.lang.Math.round( performance.startTime.getTime() + phi * performance.duration * 1000L )
			def perfEnv = performance.performanceEnvironment
			// schedule the PlayClosingJob Quartz job for the Golden Section of the piece
			Date closeDate = new Date( closingTimestamp )
			def jobParams = [
				'oscRemoteHost' : perfEnv.oscRemoteHost,
				'oscRemotePort' : perfEnv.oscRemotePort,
				'applicationId' : perfEnv.applicationId
			]
			log.info("Starting performance on SuperCollider Engine...")
			StartPerformanceJob.schedule( new Date() , jobParams )
			log.info("Scheduling performance close trigger for ${closeDate}")
			PlayCloseEventJob.schedule( closeDate, jobParams)
			log.info("Scheduling performance close trigger for ${closeDate}")
			RecordSamplesJob.schedule( new Date(), jobParams)
			performance.save()
		} catch(e) {
			def errorString =  e.cause
			log.error("unable to start Performance ${performance.name}: ${e.cause}")
		}
	}
	
	void testPerformance( Performance performance) {
		try {
			def panRange = Math.abs( performance.panMax - performance.panMin )
			def index = 0
			// Golden Ratio
			def phi = 0.618034
			// closing timestamp is current timestamp + (phi * performance duration)
			def perfEnv = performance.performanceEnvironment
			// schedule the PlayClosingJob Quartz job for the Golden Section of the piece
			Date closeDate = new Date( closingTimestamp )
			def jobParams = [
				'oscRemoteHost' : perfEnv.oscRemoteHost,
				'oscRemotePort' : perfEnv.oscRemotePort,
				'applicationId' : perfEnv.applicationId
			]
			log.info("Testing performance on SuperCollider Engine...")
			TestPerformanceJob.schedule( new Date() , jobParams )
		} catch(e) {
			def errorString =  e.cause
			log.error("unable to start Performance ${performance.name}: ${e.cause}")
		}
	}

}
