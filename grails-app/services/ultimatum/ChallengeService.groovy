package ultimatum

import java.util.List;

import grails.plugin.jms.JmsService
import grails.util.Environment

import javax.jms.MapMessage
import javax.jms.Message

import grails.util.Holders

//import org.codehaus.groovy.grails.commons.ApplicationHolder

class ChallengeException extends RuntimeException {
	String message
	Challenge challenge
}

class ChallengeService {

	def challengeManager
	def performanceEnvironmentService
	def receivedChallenge
	def jmsService
	def grailsApplication
	def challengeQueueName = "Ultimatum.challenge." + Environment.current.name
	def oscManager
	
	def ChallengeService() {
	}
	
    def sendChallengeMQ( Performer sourcePerformer, Performer targetPerformer, String messageType, String messageBody) {
		def challenge = new Challenge( source : sourcePerformer, target : targetPerformer, type : messageType, message : messageBody )
		if ( challenge.validate() ) {
			// put the Challenge on the Message Queue
			// def challengeMessage = [ source : sourcePerformer, target : targetPerformer, type : messageType, message : messageBody ]
			def challengeMap = new HashMap( source : sourcePerformer.name, target : targetPerformer.name, type : messageType, message : messageBody )
			//sendJMSMessage( grailsApplication.metadata['app.name'] + ".challenge", challengeMap)
			// hardcode appName until I can figure out where to get it.
			log.info( "Sending challenge to queue ${challengeQueueName}")
			//def msg = sendJMSMessage( challengeQueueName, challengeMap)
			def msg = sendJMSMessage( challengeQueueName, challenge)
			oscManager.sendChallengeMessage( targetPerformer.id )
			return msg
		} else {
			log.info("Could not validate challenge: ${challenge.errors}")
			//def challengeExc = new ChallengeException( "Could not validate challenge", challenge)
		}
    }
	
	def Challenge sendChallenge( Long sourcePerformerId, Long targetPerformerId, String messageType, String messageBody ) {
		Performer source = Performer.get(sourcePerformerId)
		Performer target = Performer.get(targetPerformerId)
		if ( source != null && target != null ) {
			def challenge = new Challenge( source : source, target : target, type : messageType, message : messageBody )
			if ( challenge.validate() ) {
				challenge.save()
			} else {
				log.info("Unable to save challenge from ${source.name} to ${source.name}")
				challenge.getErrors().each { error ->
					log.error("Error: ${error}")
				}
			}
			Performance perf = challenge.source.performance
			def oscManager = getOSCManager( perf )
			try {
				oscManager.sendChallengeMessage( target.id, challenge.id )
			} catch (UnknownHostException exc) {
				log.error("Unable to send message to ${oscManager.remoteHost}:${oscManager.remotePort}/${oscManager.applicationId}")
				log.error(exc.getMessage())
			}
			log.info("ChallengeService sendChallenge sent challenge: '${messageBody}' from ${source.name} to performer ${target.name}")
			return challenge
		} else {
			if ( source == null ) {
				log.info("Source Performer for ${sourcePerformerId} could not be found")
			}
			if ( target == null ) {
				log.info("Target Performer for ${targetPerformerId} could not be found")
			}
		}
	}
	
	def List<Challenge> getChallengesTo( Long targetPerformerId ) {
		//jmsService = new JmsService()
		ArrayList<Challenge> challengeList = new ArrayList<Challenge>()
		Performer targetPerformer = Performer.get(targetPerformerId)
		if ( targetPerformer != null ) {
			//log.info( "Getting challenges from queue ${challengeQueueName} for target ${targetPerformer.name}")
			//List<Challenge> myChallenges = jmsService.browseSelected( challengeQueueName, "target = ${targetPerformer.name}", null)
			challengeList.addAll(Challenge.findAllByTarget( targetPerformer ))
			def challengeQuery = Challenge.where {
				target == targetPerformer && acknowledged == false
			}
			//log.info( myChallenges )
		} else {
			log.error( "Performer with id ${targetPerformerId} not found")
		}
		return challengeList
	}
	
	def List<Challenge> getChallengesFrom( Long sourcePerformerId ) {
		//jmsService = new JmsService()
		ArrayList<Challenge> challengeList = new ArrayList<Challenge>()
		Performer sourcePerformer = Performer.get(sourcePerformerId)
		if ( sourcePerformer != null ) {
			//log.info( "Getting challenges sent by ${sourcePerformer.name} ")
			//challengeList.addAll( Challenge.findAllBySource( sourcePerformer ) )
			def challengeQuery = Challenge.where {
				source == sourcePerformer && acknowledged == false
			}
			challengeList = challengeQuery.findAll()
			//log.info( myChallenges )
		} else {
			log.info( "Performer with id ${sourcePerformerId} not found")
		}
		return challengeList
	}

	def List<Challenge> getChallenges() {
		ArrayList<Challenge> challengeList = new ArrayList<Challenge>()
		challengeList.addAll( Challenge.findAll() )
		/* getChallenges does not alter the list of challenges but merely retrieves them */
		/****
		challenges.removeAll({ item ->
			item.target.name == perf.name
		})
		****/
		log.info( challengeList )
		return challengeList
	}
	
	def List<Challenge> getChallenges( Long sourcePerformerId, Long targetPerformerId ) {
		Performer sourcePerformer = Performer.get(sourcePerformerId)
		Performer targetPerformer = Performer.get(targetPerformerId)
		ArrayList<Challenge> challengeList = new ArrayList<Challenge>()
		if ( sourcePerformer != null && targetPerformer != null ) {
			/*
			challengeList.addAll( Challenge.findAllBySourceLikeAndTargetLike ( sourcePerformer, targetPerformer ) )
			*/
			/*
			 * Changing the query to retrieve only challenges that have not been responded to.
			 */
			def challengeQuery = Challenge.where {
				source == sourcePerformer && target == targetPerformer && acknowledged == false
			}
			challengeList = challengeQuery.findAll()
			/* getChallenges does not alter the list of challenges but merely retrieves them */
			log.info( challengeList )
		} else {
			log.error("Source or target performer not found: source = ${sourcePerformer}, target = ${targetPerformer}")
		}
		return challengeList
	}

	def void respondToChallenge( Long challengeId ) {
		Challenge challenge = Challenge.get(challengeId)
		if ( challenge != null ) {
			challenge.responded = true
			challenge.save()
			log.info("${challenge.target.name} responded to challenge id:${challenge.id} from ${challenge.source.name}")
			Performance perf = challenge.source.performance
			if ( perf == null ) {
				log.error("Performance reference in sourcePerformer should not be null in replyToChallenge()!")
			}
			OSCManager oscManager = getOSCManager(perf)
			oscManager.sendFinishedMessage( challenge.source.id, challenge.id )
			//challenge.delete()
		} else {
			log.error("Challenge with id ${challengeId} not found")
		}
	}

	def void puntOnChallenge( Long challengeId, Performer performer ) {
		Challenge challenge = Challenge.get(challengeId)
		if ( challenge != null ) {
			challenge.responded = true
			challenge.save()
			log.info("${challenge.source.name} punted on challenge id:${challenge.id} from ${challenge.target.name}")
			Performance perf = challenge.source.performance
			if ( perf == null ) {
				log.error("Performance reference in sourcePerformer should not be null in replyToChallenge()!")
			}
			OSCManager oscManager = getOSCManager(perf)
			// If a performer is punting on a challenge issued by another performer, the punt must be followed
			// by a response sample (one more time, I'm gonna...). In this case, we send a puntAndResponse message.
			if ( performer.id != challenge.source.id ) {
				oscManager.sendPuntAndResponse( challenge.source.id, challenge.id )
			} else {
				oscManager.sendPuntMessage( challenge.source.id, challenge.id )
			}
		} else {
			log.error("Challenge with id ${challengeId} not found")
		}
	}

	def void sendPuntMessage( Long targetPerformerId) {
		Performer target = Performer.get(targetPerformerId)
		if ( target != null ) {
			Performance perf = target.performance
			if ( perf == null ) {
				log.error("Performance reference in performer (${target.name}) should not be null in sendPuntMessage()!")
			}
			OSCManager oscManager = getOSCManager(perf)
			oscManager.sendFinishedMessage( target.id )
		} else {
			log.error("Performer with id ${targetPerformerId} not found")
		}
	}

	def void setAcknowledged( Long challengeId ) {
		Challenge challenge = Challenge.get(challengeId)
		if ( challenge != null ) {
			challenge.acknowledged = true
			challenge.save()
			log.info("${challenge.source.name} acknowledged response to challenge id:${challenge.id} from ${challenge.target.name}")
		} else {
			log.error("Challenge with id ${challengeId} not found")
		}
	}

	def void setResponded( Long challengeId ) {
		Challenge challenge = Challenge.get(challengeId)
		if ( challenge != null ) {
			challenge.responded = true
			challenge.save()
			log.info("${challenge.source.name} responded to challenge id:${challenge.id} from ${challenge.target.name}")
		} else {
			log.error("Challenge with id ${challengeId} not found")
		}
	}

	def OSCManager getOSCManager( Performance perf ) {
		def perfEnv = perf.performanceEnvironment
		if ( perfEnv != null ) {
			return new OSCManager( perfEnv.oscRemoteHost, perfEnv.oscRemotePort, perfEnv.applicationId )
		} else { // if cannot get a PerformanceEnvironment form the Performance object itself, fall back to app properties
			log.warn("Unable to retrieve PerformanceEnvironment from active performance, falling back to values from application properties.")
			return new OSCManager(
				Holders.config.ultimatum.oscRemoteHost,
				Holders.config.ultimatum.oscRemotePort,
				Holders.config.ultimatum.applicationId
			)
		}
	}
}
