package ultimatum

import org.slf4j.Logger;

class PerformerException extends RuntimeException {
	String message
	Performer performer
}

class PerformerService {

	def transactional = true
    Performer createPerformer(String name, String passcode, String position, Integer input, Performance performance) {
		def role
		// First performer created will be the leader
		if ( Performer.count() == 0 ) {
			role = "leader"
		} else {
			role = "player"
		}
		log.info("Creating new Performer  with name ${name} as ${role}")
		def performer= new Performer(name : name, passcode : passcode, position : position, audioInput : input, role : role, performance : performance)
		if ( performer.validate() ) {
			performer.save()
			log.info("Performer ${name} was saved.")
			return performer
		} else {
			//throw new PerformerException( message : "Could not validate Performer")
			performer.getErrors().each { error ->
				log.error("Error: ${error}")
			}
		}
		return performer
    }
	
	Performer createPerformer(Map params, Performance performance) {
		def role
		// First performer created will be the leader
		if ( Performer.count() == 0 ) {
			role = "leader"
		} else {
			role = "player"
		}
		log.info("Creating new Performer  with name ${params.name} as ${role}")
		def performer = new Performer(name : params.name, passcode : params.performerPasscode, position : params.position, audioInput : params.audioInput, role : role, performance : performance)
		if ( performer.validate() ) {
			performer.save()
			log.info("Performer ${performer.name} was saved.")
			return performer
		} else {
			//throw new PerformerException( message : "Could not validate Performer")
			performer.getErrors().each { error ->
				log.error("Error: ${error}")
			}
		}
		return performer
	}

	Performer validateLogin(String performerName, String performerPasscode, String performancePasscode) {
		log.info("Validating Performer ${performerName}")
		def performer = Performer.findByName( performerName )
		if ( performer == null ) {
			log.info("Performer with name ${performerName} was not found")
			return null
		}
		if ( performer.passcode != null ) {
			if ( performer.passcode != performerPasscode ) {
				log.info("Passcode ${performerPasscode} does not match passcode for : ${performerName} :: should be ${performer.passcode}")
				return null
			}
		}
		log.info("Performer ${performerName} was validated")
		return performer
	}

	def deletePerformer( Long performerId ) {
		Performer performer = Performer.get(performerId)
		try {
			if ( performer != null ) {
				performer.delete()
				session.invalidate()
			}
		} catch(e) {
			def errorString =  e.cause
			log.error("unable to delete Performer ${performer}: ${errorString}")
		}
	}
}
