<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta name="layout" content="main"/>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="Ultimatum"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
		<link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
		<link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">
		<g:layoutHead/>
    	<asset:stylesheet src="application.css"/>
    	<!-- <asset:stylesheet src="main.css"/>   -->
	</head>
	<body>
		<g:set var="performer" value="${session.performer}"/>
		<div class="navbar navbar-static-top" id="top">
   				<div class="navbar-header">
					<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#bs-navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="#" class="navbar-brand">Ultimatum</a>
				</div>
				<div class="navbar" role="navigation">				
					<ul class="nav navbar-nav" >
						<li><g:link controller="performance" action="dashboard">Dashboard</g:link></li>
						<li><g:link controller="performance" action="perform">Perform</g:link></li>
						<g:if test="${performance != null && performer == null}"><li><g:link controller="performance" action="join">Join</g:link></li></g:if>
						<g:if test="${performer != null}"><li><g:link controller="performer" action="edit">Edit</g:link></li></g:if>
						<g:if test="${performer == null}"><li><g:link controller="performance" action="login">Login</g:link></li></g:if>	
						<g:if test="${performer != null}"><li><g:link controller="performance" action="logout">Logout</g:link></li></g:if>
						<g:if test="${performance != null && performer != null && performer.role == 'leader'}"><li><g:link controller="performance" action="destroy">Destroy</g:link></li></g:if>		
						<li><g:link controller="performance" action="about">WTF is Ultimatum?</g:link></li>
					</ul>
				</div>
		</div>
		
		<div class="container">
			<div class="ultimatumLogo"><asset:image src="ultimatum-image.png"/></div>
			<g:layoutBody/>
		</div>
		<div class="footer" role="contentinfo"></div>
		<div id="spinner" class="spinner" style="display:none;"><g:message code="spinner.alt" default="Loading&hellip;"/></div>
    	<asset:javascript src="application.js"/>
	</body>
</html>
