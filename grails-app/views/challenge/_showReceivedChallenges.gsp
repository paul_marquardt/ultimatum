<g:if test="${challenges != null}">
	<div class="panel panel-success">
	<h4 class="panel-heading">${message}</h4>
	<div class="panel-body" style="margin-bottom: 12px;">
	<g:each in="${challenges}" var="challenge">
		<g:if test="${! challenge.responded}">
			<g:set var="sourceP" value="${ultimatum.Performer.get(1)}"/>
			<g:set var="dateString" value="${challenge.timestamp.format('HH:mm:ss')}"/>
					<div>Challenge from ${challenge.source.name} sent at ${dateString}</div>
					<div class="btn-group" role="group">
						<button type="button" class="btn respondToChallenge" onClick='$.post( "${request.contextPath}/challenge/respondToChallenge", { id: "${challenge.id}" } );'>Respond</button>
					</div>
					<div class="btn-group" role="group">
						<button type="button" class="btn puntOnChallenge" onClick='$.post( "${request.contextPath}/challenge/puntOnChallenge", { id: "${challenge.id}" } );'>Punt</button>
					</div>
		</g:if>
	</g:each>
	</div>
	</div>
</g:if>
<g:else>
	<div></div>
</g:else>