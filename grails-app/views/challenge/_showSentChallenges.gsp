<g:if test="${challenges != null}">
	<div class="panel panel-success">
	<h4 class="panel-heading">${message}</h4>
	
	<g:each in="${challenges}" var="challenge">
		<g:set var="sourceP" value="${ultimatum.Performer.get(1)}"/>
		<g:set var="dateString" value="${challenge.timestamp.format('HH:mm:ss')}"/>
			<div class="panel-body">
				<div>Challenge to ${challenge.target.name} sent at ${dateString}</div>
				<g:if test="${challenge.responded}">
				<div class="btn-group" role="group">
					<button type="button" class="btn puntOnChallenge" onClick='$.post( "${request.contextPath}/challenge/puntOnChallenge", { id: "${challenge.id}" } );'>Punt</button>
				</div>
				<div class="btn-group" role="group">
					<button type="button" class="btn acknowledgeChallenge" onClick='$.post( "${request.contextPath}/challenge/acknowledgeChallenge", { id: "${challenge.id}" } );'>Delete</button>
				</div>
				</g:if>
			</div>
	</g:each>
	</div>
</g:if>
<g:else>
	<div></div>
</g:else>