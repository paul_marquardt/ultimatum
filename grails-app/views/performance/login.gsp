<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII"/>
<meta name="layout" content="main"/>
<title>Ultimatum Login page</title>
</head>
<body>
  <div class="container">
  	<g:renderErrors bean="${performer?.errors}" as="list" />
  	<g:if test="$flash.message != null">
  		<div>${flash.message} }</div>
  	</g:if>
	<p>Login to Performance <strong>${performance?.name}</strong></p>
      <g:form url="[action:'login',controller:'performance']">
         <dl>
            <dt>Performer Name</dt>
            <dd><g:textField name="name"
               value="${performer?.name}"/></dd>
            <dt>Performer Passcode</dt>
            <dd><g:textField name="performerPasscode"/></dd>
            <g:if test="${performance?.passcode}">
            	<dt>Performance Passcode</dt>
            	<dd><g:passwordField name="performancePasscode"/></dd>
            </g:if>
            <dt><g:submitButton name="add" value="Login"/></dt>
         </dl>
      </g:form>
  </div>
</body>
</html>