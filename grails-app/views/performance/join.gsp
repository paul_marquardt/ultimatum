<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII"/>
<meta name="layout" content="main"/>
<title>Insert title here</title>
</head>
<body>
  <div class="container">
  	<g:renderErrors bean="${performer?.errors}" as="list" />
  	
	<h4>Join a Performance <strong style="color : blue">${performance?.name}</strong> as a new Performer</h4>
    <g:form url="[action:'join',controller:'performance']" class="form-horizontal">
    	<div class="form-group">
            <label for="name" class="col-md-2 control-label">Performer Name</label>
           	<div class="col-md-10">
       			<g:textField class="form-control" name="name" value="${performer?.name}"/>
           	</div>
    
            <label for="name" class="col-md-2 control-label">Performer Passcode</label>
           	<div class="col-md-10">
       			<g:textField class="form-control" name="performerPasscode" value="${performer?.passcode}"/>
           	</div>

            <label for="position" class="col-md-2 control-label">Position</label>
           	<div class="col-md-10">
       			<g:textField class="form-control" name="position" value="${performer?.position}"/>
           	</div>

            <g:if test="${performance?.passcode}">
            	<label for="position" class="col-md-2 control-label">Performance Passcode</label>
           		<div class="col-md-10">
       				<g:passwordField class="form-control" name="performancePasscode" value="${performance?.passcode}"/>
           		</div>
            </g:if>
        </div>
        <g:submitButton class="btn btn-lg btn-primary btn-block" name="add" value="Join Performance"/>
      </g:form>
  </div>
</body>
</html>