<div class="panel panel-success" id="getPerformers">
<div class="panel panel-body">
	<div style="margin-bottom: 12px;">
		<g:each in="${performers?}" var="performer">
			<button type="button" class="btn btn-primary sendChallenge" onClick='$.post( "${request.contextPath}/challenge/sendChallenge", { targetPerformerId: "${performer.id}" } );'>Send Challenge to <strong>${performer.name}</strong></button>
		</g:each>
	</div>
</div>
</div>