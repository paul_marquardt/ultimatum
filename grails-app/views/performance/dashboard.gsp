<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="layout" content="main"/>
<title>Ultimatum Dashboard</title>
</head>
<body>
	<div class="container">
  		<div class="row">
  			<div class="col-md-8">
  				<h3>Dashboard for Performance <strong style="color : blue">${performance?.name}</strong></h3>
  			</div>
  		</div>
  		<g:if test="${flash.message != null}">
  			<p>A Message: ${flash.message}</p>
  		</g:if>
  	
  		<g:if test="${performers?.size() == 0}">
  			<div class="row">
  				<div class="col-md-8">
  					<p>There are no performers in this performance. Would you like to <g:link controller="performance" action="join">join it</g:link>?</p>
  				</div>
  			</div>
  		</g:if>
  		<!-- if we are not a member of the Performance, prompt to join it -->
  		<g:if test="${ performers?.size() > 0 && session['performer'] == null}">
  			<div class="row">
  				<div class="col-md-8">
  					<p>You are not a member of this performance. Would you like to <g:link controller="performance" action="join">join it</g:link>?</p>
  				</div>
  			</div>			
  		</g:if>
  		<g:else>
			<g:render template="/modules/showPerformers" model="['performance': performance, 'performers': performers]" />
  		</g:else>
  		<g:if test="${session['performer']?.role == 'leader' && performance?.startTime == null}">
  		<div class="row">
    		<g:form action="start">
				<g:submitButton class="btn btn-lg btn-primary btn-block" name="start" value="Start Performance"/>
			</g:form>
		</div>
  		<div class="row">
    		<g:form action="test">
				<g:submitButton class="btn btn-lg btn-primary btn-block" name="test" value="Test Performance"/>
			</g:form>
		</div>
    	</g:if>
  	</div>
</body>
</html>