<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII"/>
	<meta name="layout" content="main"/>
	<title>Main Performance Page</title>
	<g:javascript library="jquery" plugin="jquery"/>
</head>
<body>
	<script>
	var appRoot = "${request.contextPath}";
	var sendChallengeId = "#getPerformers";
	var sendChallengeURI = appRoot + "/performance/getPerformers";
	var receivedChallengesId = "#receivedChallenges";
	var receivedChallengesURI = appRoot + "/challenge/getReceivedChallenges";
	
	var getPerformanceTimeURI = appRoot + "/performance/performanceElapsedTime";
	var performanceTimeId = "#performanceTime";
	
	var pollSendChallenge = function () {
		$(sendChallengeId).load( sendChallengeURI )
	};

	var pollReceivedChallenges = function () {
		$(receivedChallengesId).load( receivedChallengesURI )
	};

	var sentChallengesId = "#sentChallenges";
	var sentChallengesURI = appRoot + '/challenge/getSentChallenges';
	
	var pollSentChallenges = function () {
		$(sentChallengesId).load( sentChallengesURI )
	};

	var pollPerformanceElapsedTime = function () {
		$(performanceTimeId).load( getPerformanceTimeURI );
	};
	
	/*
	pollreceivedChallenges and pollSentChallenges should be updated
	at tight interval to ensure challenges are processed quickly
	*/
	setInterval(pollReceivedChallenges, 1000);
	setTimeout(pollReceivedChallenges, 800);

	setInterval(pollSentChallenges, 1000);
	setTimeout(pollSentChallenges, 800);

	setInterval(pollPerformanceElapsedTime, 1000);
	setTimeout(pollPerformanceElapsedTime, 800);
	
	/*
	pollSendChallenge doesn't need very tight update schedule, as it only changes when a performer joins or leaves
	*/
	setInterval(pollSendChallenge, 2000);
	setTimeout(pollSendChallenge, 1500);
	
	
	//setInterval(function() { console.log("fuck you"); }, 1000);
	</script>
	<div class="container">
		<div class="header">
			<h3>Ultimatum main performance interface</h3>
		</div>
		<div class="row">			
  			<div class="col-md-4 form-group" id="performanceTime">
				<g:if test="${session.performer}">
					<div id="performanceTime"></div>
				</g:if>  				
			</div>
		</div>
		<div class="row">			
  			<div class="col-md-4 form-group" id="challengeResults">
  				<h3>Send a challenge</h3>
				<g:if test="${session.performer}">
					<div id="getPerformers"></div>
				</g:if>  				
			</div>
			

			<div class="col-md-4 form-group" id="challengeResults">
  				<h3>My sent Challenges</h3>
				<g:if test="${session.performer}">
					<div id="sentChallenges"></div>
				</g:if>
			</div>
	
			<div class="col-md-4 form-group" id="challengeResults">	
  				<h3>Challenges sent to me</h3>
				<g:if test="${session.performer}">
					<div id="receivedChallenges"></div>
				</g:if>
			</div>
		</div>
	</div>
</body>
</html>