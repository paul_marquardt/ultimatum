<html>
   <head>
      <title>Create New Performance</title>
      <meta name="layout" content="main"/>
   </head>
   <body>

	 <div class="container" id="performanceView">

  	 <g:if test="${flash.message != null}">
  		<p><strong>${flash.message}</strong></p>
  	 </g:if>

     <g:hasErrors>
       <div class="errors">
         <g:renderErrors bean="${performance}" as="list" />
       </div>
     </g:hasErrors>
	 <g:if test="${flash.message != null}"><div>${flash.message}</div></g:if>
     <g:form action="create" class="form-horizontal">
		<h2 class="row" class="form-signin-heading">Create a new Performance</h2>
		<div class="form-group">
            <label for="name" class="col-md-2 control-label">Performance Name</label>
           	<div class="col-md-10">
       			<g:textField class="form-control" name="name" value="${performance?.name}"/>
           	</div>
            	
           	<label for="duration" class="col-md-2 control-label">Duration (in seconds)</label>
            <div class="col-md-10">
	            <g:textField class="form-control" name="duration" value="${performance?.duration}"/>
	        </div>
	            
            <label for="passcode" class="col-md-2 control-label">Performance Passcode</label>
            <div class="col-md-10">
            	<g:textField class="form-control" name="passcode" value="${performance?.passcode}"/>
            </div>
		</div>
            
		<div class="form-group">
            <label for="oscRemoteHost" class="col-md-2 control-label">OSC Remote Host</label>
            <div class="col-md-offset-2">
            	<g:textField class="form-control" name="oscRemoteHost" value="${oscRemoteHost}"/>
            </div>
            
            <label for="oscRemotePort" class="col-md-2 control-label">OSC Remote Port</label>
            <div class="col-md-offset-2">
 				<g:textField class="form-control" name="oscRemotePort" value="${oscRemotePort}"/>
			</div>
				
            <label for="applicationId" class="col-md-2 control-label">Application Id</label>
            <div class="col-md-offset-2">
 				<g:textField class="form-control" name="applicationId" value="${applicationId}"/>
 			</div>
        </div>
        <g:submitButton class="btn btn-lg btn-primary btn-block" name="create" value="Create Performance"/>
     </g:form>
     </div>
   </body>
</html>