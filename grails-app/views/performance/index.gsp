<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="layout" content="main"/>
<title>Ultimatum Performance controller</title>
</head>
<body>
  <div class="body">
  	<g:if test="${performance == null}">
  		<div class="row">
  			<div class="col-md-4">
  				<h4>There is no active performance. Do you want to <g:link controller="performance" action="create">create one?</g:link></h4>
  			</div>
  		</div>
  	</g:if>
  	<g:elseif test="${session.performer == null}">
 		<div class="row">
  			<div class="col-md-4">
  				<h4>There is an active Performance <strong>${performance?.name}</strong>. Would you like to <g:link controller="performance" action="join">join it</g:link>?</h4>
  			</div>
  		</div>
  	</g:elseif>
  </div>
</body>
</html>