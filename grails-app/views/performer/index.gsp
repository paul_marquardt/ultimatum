<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="layout" content="main"/>
<title>Ultimatum Performer interface</title>
</head>
<body>
  <div class="body">
  <g:if test="${performance == null}">There are no active Performances, please
  	<g:link controller="performance" action="create">create one</g:link>
  </g:if>
  <g:elseif test="${session['performer'] == null}">
    <div>You are not a member of any Performance. There is an active Performance <strong>${performance?.name}</strong>.
    Would you like to <g:link controller="performance" action="dashboard">join it</g:link>?</div>
  </g:elseif>
  <g:else>
  	
  </g:else>
  </div>
</body>
</html>