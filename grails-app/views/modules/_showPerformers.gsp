<div class="container">
	<g:if test="${performance.startTime != null}">
		<div class="row">
			<div class="col-md-6">
				<h3>Performance '${performance.name}' has been started at ${performance.startTime.toString()}</h3>
			</div>
		</div>
	</g:if>
	
	<div class="row">
		<div class="col-md-6">
			<g:if test="${performers?.size() > 0}">
			<h4>Performers in this performance:</h4>
			<g:each in="${performers?}" var="perf">
				<ul>
					<li><span><strong>${perf.name}</strong></span><g:if test="${perf.role == 'leader'}"> (Leader)</g:if></li>
					<li style="margin-left: 16px">Position: ${perf.position}</li>
				</ul>
			</g:each>
			</g:if>
			<g:else>
				<h4>There are no performers in this performance.</h4>
			</g:else>
		</div>
	</div>
</div>