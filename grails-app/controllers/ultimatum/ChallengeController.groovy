package ultimatum

import java.util.Random

class ChallengeController {

	def challengeService
	def challengeManager
	
	static defaultAction = "perform"
		
	def sendChallenge() {
		//challengeService = new ChallengeService()
		def message
		def type
		def sentChallenge
		log.info("[${controllerName}@${actionName}] :: Sent params: ${params}")
		if ( params.message == null ) {
			message = "A challenge"
		} else {
			message = params.message
		}
		if ( params.type == null ) {
			type = "ultimatum.challenge"
		} else {
			type = params.type
		}
		//challengeService.sendChallenge(sourceP, targetP, type, message)
		//sentChallenge = challengeService.sendChallengeMQ(sourceP, targetP, type, message)
		
		sentChallenge = challengeService.sendChallenge(session["performer"].id, Long.parseLong (params.targetPerformerId), type, message)
		/*
		 * This should not be necessary, since the Javascript should take care of updating statues.
		if ( sentChallenge != null ) {
			render (template : "sendChallenge", model : [
					source : sentChallenge.source,
					target : sentChallenge.target,
					message : sentChallenge.message,
					id : sentChallenge.id
				]
			)
		} else {
			render (template : "sendChallenge", model : [ source : session["performer"], target : Performer.get(params.targetPerformerId), message : "Unable to send challenge" ])
		}
		*/
	}

	def getReceivedChallenges() {
		//challengeService = new ChallengeService()
		//challenges = challengeService.getChallenges(session.performer)
		if ( session.performer == null ) {
			return render (template : "showReceivedChallenges", model : [ challenges : null, message : "You must be logged on to use this function"])
		}
		def challenges = challengeService.getChallengesTo(session.performer.id)
		/*
		 * remove unresponded challenges.
		 *
		 */
		def challengeCount = challenges.size()
		if ( challengeCount > 2 ) {
			for ( challenge in challenges.sort{ it.id } )
				{
					if ( challenge.id < ( challengeCount - 2 ) ) {
					challenges.remove( challenge.id )
					}
				}
		}
		def message
		if ( challenges.empty ) {
			message = "No challenges sent to ${session.performer.name}"
		} else {
			message = "Challenges sent to ${session.performer.name} <span class='badge'>${challenges.size()}</span>"
		}
		return render (template : "showReceivedChallenges", model : [ challenges : challenges, message : message])
	}

	def getSentChallenges() {
		//challengeService = new ChallengeService()
		//challenges = challengeService.getChallenges(session.performer)
		if ( session.performer == null ) {
			return render (template : "showSentChallenges", model : [ challenges : null, message : "You must be logged on to use this function"])
		}
		def challenges = challengeService.getChallengesFrom(session.performer.id)
		def message
		if ( challenges.empty ) {
			message = "No challenges sent by ${session.performer.name}"
		} else {
			def numChallenges = challenges.size()
			message = "Challenges sent by ${session.performer.name} <span class='badge'>${challenges.size()}</span>"
		}
		return render (template : "showSentChallenges", model : [ challenges : challenges, message : message])
	}


	def respondToChallenge() {
		//challengeService = new ChallengeService()
		if ( params.id != null ) {
			def challengeId = Integer.parseInt(params.id)
			//challengeService.sendChallenge(sourceP, targetP, type, message)
			challengeService.respondToChallenge( challengeId )
			def challenge = Challenge.get(challengeId)
			if ( challenge != null ) {
				log.error("Unable to find challenge with id ${params.id}.")
			}
		} else {
			log.info("No id parameter supplied.")
		}
	}

	def puntOnChallenge() {
		//challengeService = new ChallengeService()
		if ( params.id != null && session["performer"] != null ) {
			def challengeId = Integer.parseInt(params.id)
			challengeService.puntOnChallenge( challengeId, session["performer"] )
			/*
			 * I would prefer to do this in the service layer, but I'm not sure if ChallengeService has access
			 * to the "session" Map.
			 * Logic is simple: If a performer is punting on a challenge that originated from them,
			 * we can safely delete it, since, as per the interface, a performer can only punt on
			 * their own challenge if the target performer responded.
			 * 
			 */
			def challenge = Challenge.get(challengeId)
			if ( challenge != null && session["performer"] != null && challenge.source.id == session["performer"].id ) {
				log.info("Performer ${session['performer'].name} punted on their own challenge. Challenge with id ${challenge.id} will be deleted.")
				challenge.delete()
			}
		}
	}

	def acknowledgeChallenge() {
		//challengeService = new ChallengeService()
		if ( params.id != null && session["performer"] != null ) {
			def challengeId = Integer.parseInt(params.id)
			challengeService.setAcknowledged( challengeId )
			/*
			 * I would prefer to do this in the service layer, but I'm not sure if ChallengeService has access
			 * to the "session" Map.
			 * Logic is simple: If a performer is punting on a challenge that originated from them,
			 * we can safely delete it, since, as per the interface, a performer can only punt on
			 * their own challenge if the target performer responded.
			 *
			 */
			def challenge = Challenge.get(challengeId)
			if ( challenge != null && session["performer"] != null && challenge.source.id == session["performer"].id ) {
				log.info("Performer ${session['performer'].name} acknowledged their challenge. Challenge with id ${challenge.id} will be deleted.")
				challenge.delete()
			}
		}
	}

	def getRandomList() {
		def rand = new Random()
		def listSize = 10
		ArrayList randList = new ArrayList<Map>()
		(0 .. listSize).each { item ->
			Expando exp = new Expando()
			exp.id = item
			exp.value = rand.nextInt(1000)
			randList.add( [ id : exp.id, value : exp.value ] )
		}
		// render a JSON ( http://www.json.org ) response with the builder attribute:
		render(contentType: "application/json") { randList }
		//render (template : "showChallengesTest", model : [ mapList : randList ])
		
	}
}
