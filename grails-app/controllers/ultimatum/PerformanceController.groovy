package ultimatum

import org.slf4j.Logger;
import grails.util.Holders
import grails.converters.*

class PerformanceController {
		
    def index() {
		def performanceService = new PerformanceService()
		def performance = performanceService.getPerformance()
		if ( performance == null ) {
			session.performer = null
		}
		[ performance : performance ]
	}
	
	def create() {
		def performanceService = new PerformanceService()
		def performance = performanceService.getPerformance()
		if ( performance != null ) {
			flash.message = "A Performance has already been created"
			render( view : "dashboard", model : [ performance : performance ] )
		} else {
			if ( params.name == null || params.duration == null ) {
				return render( view : "create", model : [
					oscRemoteHost : Holders.config.ultimatum.osc.remoteHost,
					oscRemotePort : Holders.config.ultimatum.osc.remotePort,
					applicationId : Holders.config.ultimatum.osc.applicationId
				] )
			}
			//performance = performanceService.createPerformance(params.name, params.passcode, params.double('duration') )
			performance = performanceService.createPerformance( params )
			if ( performance.hasErrors() ) {
				log.info("[${controllerName}@${actionName}] :: Performance has errors: ${performance.getErrors()}.")
				return redirect( view : "dashboard", model : [ performance : performance, performers : performance.getPerformers() ])
			} else {
				log.info("[${controllerName}@${actionName}] :: Performance with name '${performance.name}' is validated.")
				return redirect( view : "dashboard", model : [ performance : performance, performers : performance.getPerformers() ])
			}
		}
	}
	
	def dashboard() {
		def performanceService = new PerformanceService()
		def performance = performanceService.getPerformance()
		if ( performance != null ) {
			//log.info("[${controllerName}@${actionName}] :: Performers in '${performance.name}':")
			performance.getPerformers().each { performer ->
				log.info("\tPerformer: ${performer.name}")
			}
			return render ( view : "dashboard", model : [ performance : performance, performers : performance.getPerformers()] )
		} else {
			return redirect( view : "create")
		}
	}

	def perform() {
		def performanceService = new PerformanceService()
		def performance = performanceService.getPerformance()
		if ( performance == null ) {
			return redirect( view : "create" )
		}
		if ( performance.startTime == null ) {
			flash.message = "The Performance must be started before challenges can be sent."
			return render( view : "dashboard", model : [ performance : performance, performers : performance.getPerformers() ])
		}
		def thisPerformer = session["performer"]
		if ( performance == null ) {
			return redirect(controller: "Performance", action : "create")
		}
		if ( thisPerformer == null ) {
			return redirect(controller: "Performance", action : "join")
		}

		if ( performance != null && thisPerformer != null ) {
			def performers = Performer.findByNameNotEqual( thisPerformer.name )
			[ performance : performance, performers : performers ]
		}
	}

	def getPerformers() {
		def performanceService = new PerformanceService()
		def performance = performanceService.getPerformance()
		if ( performance == null ) {
			return render (template : "getPerformers", model : [ performers : null, message : "An active performance could not be found."])
		}
		def thisPerformer = session["performer"]
		if ( thisPerformer == null ) {
			return render (template : "getPerformers", model : [ performers : null, message : "You must be logged on to use this function."])
		}
		def performers = Performer.findByNameNotEqual( thisPerformer.name )
		return render (template : "getPerformers", model : [ performers : performers])
	}

	def getSentChallenges() {
		//challengeService = new ChallengeService()
		//challenges = challengeService.getChallenges(session.performer)
		if ( session.performer == null ) {
			return render (template : "showSentChallenges", model : [ challenges : null, message : "You must be logged on to use this function"])
		}
		def challenges = challengeService.getChallengesFrom(session.performer.id)
		def message
		if ( challenges.empty ) {
			message = "No challenges sent by ${session.performer.name}"
		} else {
			def numChallenges = challenges.size()
			message = "Challenges sent by ${session.performer.name} <span class='badge'>${challenges.size()}</span>"
		}
		return render (template : "showSentChallenges", model : [ challenges : challenges, message : message])
	}
	
	def addPerformer() {
		def performanceService = new PerformanceService()
		def performance = performanceService.getPerformance()
		if ( performance != null ) {
			// there is only one active performance at any time
			def performerService = new PerformerService()
			def newPerformer =  performerService.createPerformer(params, performance)
			if ( newPerformer.validate() ) {
				log.info("[${controllerName}@${actionName}] :: Adding performer [${newPerformer.name}] to performance ${performance.name}")
				performance.addToPerformers(newPerformer)
				return render( view : "perform", model : [ performance : performance, performer : newPerformer ] )
			} else {
				return render( view : "addPerformer", model : [ performance : performance ] )
			}
			
		} else {
			render( view : "create")
		}
	}
	
	def join() {
		log.info("In [${controllerName}@${actionName}]")
		def performanceService = new PerformanceService()
		def performance = performanceService.getPerformance()
		if ( performance != null ) {			
			def performerService = new PerformerService()
			if ( params.name == null ) {
				return render( view : "join", model : [ performance : performance] )
			}
			//def newPerformer =  performerService.createPerformer(params.name, params.passcode, params.position, params.input, performance)
			def newPerformer =  performerService.createPerformer(params, performance)
			if ( newPerformer.validate() ) {
				log.info("[${controllerName}@${actionName}] :: Adding performer [${newPerformer.name}] to performance ${performance.name}")
				//performance.addToPerformers(newPerformer)
				session["performer"] = newPerformer
				def partners = Performer.findByNameNotEqual( newPerformer.name )
				//return render( view : "perform", model : [ performance : performance, performers : partners ])
				chain( action : "perform", model : [ performance : performance, performers : partners ] )
			} else {
				return render( view : "join", model : [ performance : performance, performer : newPerformer ])
			}
		} else {
			log.info("[${controllerName}@${actionName}] :: There is no Performance object")
			render( view : "create")
		}
	}

	def performanceTime() {
		def performanceService = new PerformanceService()
		def performance = performanceService.getPerformance()
		if ( performance != null ) {
			if ( performance.isStarted ) {
				render(contentType: "application/json") {
					[ "performanceStartTime" : performance.startTime.getTime() /1000L ]
				}
			} else {
				render(contentType: "application/json") {
					[ "performanceStartTime" : -9999 ]
				}
			}
		}
	}

	def performanceElapsedTime() {
		def performanceService = new PerformanceService()
		def performance = performanceService.getPerformance()
		def now = new Date()
		//log.info("Inside [${controllerName}@${actionName}]")
		if ( performance != null ) {
			def elapsedTime = (performance.duration - ( now.getTime() /1000L - performance.startTime.getTime() /1000L )).trunc()
			//if ( performance.isStarted ) {
			//log.info("The performance time is ${elapsedTime}")
			return render (template : "performanceElapsedTime", model : [ performance : performance, elapsedTime : elapsedTime ])
			//} else {
			//	render(contentType: "application/json") {
			//		[ "timeRemaining" : -9999 ]
			//	}
			//}
		}
	}

	def start() {
		def performanceService = new PerformanceService()
		def performance = performanceService.getPerformance()
		if ( performance != null ) {
			if ( performance.startTime != null ) {
				flash.message = "The performance ${performance.name} is already running"
				log.info("The performance ${performance.name} is already running")
				return render( view : "dashboard", model : [ performance : performance, performers : performance.getPerformers() ] )
			}
			// there is only one active performance at any time
			flash.message = "The performance ${performance.name} has been started"
			log.info("The performance ${performance.name} has been started")
			performanceService.startPerformance(performance)
			render( view : "dashboard", model : [ performance : performance, performers : performance.getPerformers() ] )
			//chain( action : "dashboard", model : [ performance : performance, performers : performance.getPerformers() ] )
		} else {
			render("index")
		}
	}

	def test() {
		def performanceService = new PerformanceService()
		def performance = performanceService.getPerformance()
		if ( performance != null ) {
			if ( performance.startTime != null ) {
				flash.message = "The performance ${performance.name} is already running"
				log.info("The performance ${performance.name} is already running")
				return render( view : "dashboard", model : [ performance : performance, performers : performance.getPerformers() ] )
			}
			// there is only one active performance at any time
			flash.message = "Testing performance ${performance.name}"
			log.info("Testing performance ${performance.name}")
			performanceService.testPerformance(performance)
			render( view : "dashboard", model : [ performance : performance, performers : performance.getPerformers() ] )
			//chain( action : "dashboard", model : [ performance : performance, performers : performance.getPerformers() ] )
		} else {
			render("index")
		}
	}

	def destroy() {
		def performanceService = new PerformanceService()
		def performance = performanceService.getPerformance()
		if ( performance != null ) {
			performance.delete()
			session.invalidate()
			//servletContext.challenges = null
			log.info("Performance with id ${performance.id} was deleted.")
		}
		//render( view : "dashboard", model : [ performance : null ] )
		return redirect( view : "dashboard")
	}
	
	def login() {
		def performanceService = new PerformanceService()
		def performerService = new PerformerService()
		def performance = performanceService.getPerformance()
		if ( performance == null ) {
			return redirect( view : "create" )
		}
		//def performer = Performer.findByName( params.name )
		def performer = performerService.validateLogin(params.name, params.performerPasscode, params.performancePasscode)
		if ( performer != null ) {
				session["performer"] = performer
				def partners = Performer.findByNameNotEqual( performer.name )
				//return render( view : "perform", model : [ performance : performance, performers : partners ])
				//return redirect( view : "perform" )
				chain( action : "perform", model : [ ] )
	
		}
		//render( view : "dashboard", model : [ performance : null ] )
		flash.message = "Unable to login to performance ${performance}. Please check you username and password."
		[ performance : performance ]
	}
	
	def logout() {
		def performanceService = new PerformanceService()
		def performance = performanceService.getPerformance()
		//Performer.delete( session.performer )
		session.performer = null
		return redirect( view : "index" )
	}
}