package ultimatum

import org.springframework.web.servlet.ModelAndView

class PerformerController {

    def index() { }
	
	def create() {
		def performanceService = new PerformanceService()
		def performance = performanceService.getPerformance()
		if ( performance == null ) {
			return redirect( controller:"performance", action:"index" )
		}
		def performerService = new PerformerService()
		def newPerformer =  performerService.createPerformer(params, performance)
		log.info("Adding performer [${newPerformer.name}] to performance ${performance.name}")
		if ( newPerformer.validate() ) {
			performance.addToPerformers(newPerformer)
			session["performer"] = newPerformer
			return new ModelAndView("/performance/interface", [ performance : performance ])
		} else {
			return render( view: "create", [ performer : newPerformer ])
		}
	}
	
	def delete() {
		def performanceService = new PerformanceService()
		def performance = performanceService.getPerformance()
		if ( performance == null ) {
			return redirect( controller:"performance", action:"index" )
		}
		def performerService = new PerformerService()
		Performer thisPerformer = session["performer"]
		if ( thisPerformer != null) {
			// Allow performer to delete him/herself
			if ( thisPerformer.id == params.id ) {
				performerService.deletePerformer(params.id)
				return render( view: "dashboard", [ performer : thisPerformer ])
			}
			if ( thisPerformer.role == "leader" ) {
				performerService.deletePerformer(params.id)
				return render( view: "dashboard", [ performer : thisPerformer ])
			}
		} else {
			flash.message = "You must be logged in to perform this function."
		}
	}
}
